<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <title>Custom Plastic Cards - NZ</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
    <style>

    </style>
</head>

<body>
    <div style="max-width:640px; margin:0px auto; border: 1px solid #878786; text-align:center; background:#FFF; position:relative; padding-bottom: 0px;">
        <table width="100%" border="0" cellspacing="10" cellpadding="10">
            <tr>
                
                <td style="background:#000;"><img src="<?php echo url('/').'/public/frontend/images/customcardlogo1.png';?>"></td>
             
            </tr>
            <tr>
                <td align="center">
                    <p style="color:#2b2a29; font-family: 'Open Sans', sans-serif; font-size:12.5px; padding:0px; margin-bottom:5px;  margin-top:0px;  line-height:22px;"><font face="'Open Sans', sans-serif">
		
						<table>
							
							
							<tr>
								<td>Hello <?php echo $content['firstname'];?>,<br><br>Thank you very much for your interest in our plastic cards. We will respond promptly to your enquiry. We are recognized for our low pricing, friendly service and fast turnaround <a href="https://plastic-cards.co.nz/gallery.html">(https://plastic-cards.co.nz/gallery.html)</a>.  Please also feel free to contact us at 0800 022 737 (free call), thank you again for your interest in our plastic cards.<br><br>Kind Regards<br>Hesham<br><br>Plastic Card Customization Ltd<br>www.plastic-cards.co.nz<br>info@plastic-cards.co.nz<br>Level 26, PWC Tower 188 Quay Street<br> Auckland 1010<br>Phone 0800 022 737<br>
								</td>
							</tr>


					   
						</table>
</font>
                    </p>
                </td>
            </tr>
        </table>
    </div>
</body>


</html>