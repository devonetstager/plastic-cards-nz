<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <title>Custom Plastic Cards - NZ</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
    <style>

    </style>
</head>

<body>
    <div style="max-width:640px; margin:0px auto; border: 1px solid #878786; text-align:center; background:#FFF; position:relative; padding-bottom: 0px;">
        <table width="100%" border="0" cellspacing="10" cellpadding="10">
            <tr>
                
                <td style="background:#000;"><img src="<?php echo url('/').'/public/frontend/images/customcardlogo1.png';?>"></td>
             
            </tr>
            <tr>
                <td align="center">
                    <p style="color:#2b2a29; font-family: 'Open Sans', sans-serif; font-size:12.5px; padding:0px; margin-bottom:5px;  margin-top:0px;  line-height:22px;"><font face="'Open Sans', sans-serif">
		
						<table>
							<tr>
								<td colspan="3"><strong>The following information has been submitted from your website:</strong></td>
								
							</tr>
							<tr>
								<td colspan="3">&nbsp;</td>
								
							</tr>

							<tr>
								<td>Name</td>
								<td> : </td>
								<td><?php echo $content['first_name']." ".$content['last_name'];?></td>					
							</tr>

							<tr>
								<td>Email</td>
								<td> : </td>
								<td><?php echo $content['email_id'];?></td>					
							</tr>
							<tr>
								<td>Inquiry Type</td>
								<td> : </td>
								<td><?php echo $content['inquiry_type'];?></td>					
							</tr>
							<tr>
								<td>Enquiry</td>
								<td> : </td>
								<td><?php echo $content['inquiry'];?></td>					
							</tr>

							
						
						
					   
					   
						</table>
</font>
                    </p>
                </td>
            </tr>
        </table>
    </div>
</body>


</html>