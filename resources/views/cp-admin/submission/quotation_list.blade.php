@extends('adminlte::page')

@section('title', 'Quotation Submission')

@section('content_header')
    <h1> Quotation Submission</h1>
@stop

@section('content')

    <div class="col-md-12">
        @include('layouts.alert')
		
		<div class="box">
			<div class="box-header">
				<div class="row">
					<div class="col-sm-10"><h3 class="box-title">Quotation Submission</h3></div>
					
				</div>
				
				
				
			</div>
			
			<!-- /.box-header -->
			<div class="box-body">
				<div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
					<div class="row">
						<div class="col-sm-6"></div>
						<div class="col-sm-6"></div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<table id="" class="table table-bordered " role="grid" aria-describedby="example2_info">
								<thead>
									<tr role="row" class="odd">
										<td class="sorting_1" width="5%">No</td>
										<td width="20%">Name</td>
										<td width="20%">Email</td>
										<td width="15%">Phone</td>
										<td width="30%">Comment</td>
										
										<td width="10%">&nbsp;</td>
									</tr>
								</thead>
								<tbody>
									<?php
									$page_no = 0;
									foreach($data as $details)
									{
										$page_no++;
									?>
									<tr role="row" class="odd">
										<td rowspan="2"><?php echo $page_no;?></td>
										<td><?php echo $details->qt_firstname." ".$details->qt_lastname; ?></td>	
										<td><?php echo $details->qt_email;?></td>
										<td><?php echo $details->qt_phone;?></td>
										<td><?php echo $details->qt_comments; ?></td>										
											
										<td rowspan="2" class="odd"><a href="{{URL::route('quote_submissions_delete',Crypt::encrypt($details->qt_id))}}"><i class="fa fa-fw fa-close"  title="Delete"></i></a></td>
									</tr>
									<tr role="row">
										
										<td colspan="4">
											<?php 
		echo "<strong>Card Type : </strong> ".$details->qt_card_type."<br />".
			 "<strong>No Cards : </strong> ".$details->qt_no_of_cards."<br />".
			 "<strong>Features : </strong> ".$details->qt_features."<br />".
			 "<strong>Card Thickness : </strong> ".$details->qt_cardthickness."<br />".
			 "<strong>Number of colors front : </strong> ".$details->qt_no_of_colors_front."<br />".
			 "<strong>Number of colors back : </strong> ".$details->qt_no_of_colors_back."<br />".
			 "<strong>Magnetic Stripe : </strong> ".$details->qt_magnetic_stripe."<br />".
			 "<strong>Date Card : </strong> ".$details->qt_date_card."<br />".
			 "<strong>Enquired On : </strong> ".date("d-m-Y h:i:s a",strtotime($details->created_at))."<br />"
			 ;
			 
			 
												
											?>
										</td>										
										
									</tr>
									<?php
									}
									?>
							
									
								</tbody>
								
								
							</table>
							<?php echo $data->render(); ?>
						</div>
					</div>
				   
				</div>
			</div>
			<!-- /.box-body -->
		</div>


		
		  
          <!-- /.box -->
        
	</div>

@endsection
