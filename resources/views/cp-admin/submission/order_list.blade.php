@extends('adminlte::page')

@section('title', 'Order Submission')

@section('content_header')
    <h1> Order Submission</h1>
@stop

@section('content')

    <div class="col-md-12">
        @include('layouts.alert')
		
		<div class="box">
			<div class="box-header">
				<div class="row">
					<div class="col-sm-10"><h3 class="box-title">Order Submission</h3></div>
					
				</div>
				
				
				
			</div>
			
			<!-- /.box-header -->
			<div class="box-body">
				<div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
					<div class="row">
						<div class="col-sm-6"></div>
						<div class="col-sm-6"></div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
								<thead>
									<tr role="row" class="odd">
										<td class="sorting_1" width="5%">No</td>
										<td width="15%">Name</td>
										<td width="15%">Email</td>
										<td width="15%">Phone</td>
										<td width="25%">Comments</td>
										<td width="10%">Uploaded File</td>
										<td width="10%">Enquired On</td>
										
										<td width="5%">&nbsp;</td>
									</tr>
								</thead>
								<tbody>
									<?php
									$page_no = 0;
									foreach($data as $details)
									{
										$page_no++;
									?>
									<tr role="row" class="odd">
										<td><?php echo $page_no;?></td>
										<td><?php echo $details->sbo_firstname." ".$details->sbo_lastname; ?></td>	
										<td><?php echo $details->sbo_email;?></td>
										<td><?php echo $details->sbo_phone;?></td>
										<td><?php echo $details->sbo_comments; ?></td>		
										<td><a target="_blank" download href="<?php echo asset('public/formsubmission/'.$details->sbo_upload);?>">DOWNLAOD</a></td>										
										<td><?php echo date("d-m-Y h:i:s a",strtotime($details->con_date)); ?></td>		
										<td><a href="{{URL::route('order_submissions_delete',Crypt::encrypt($details->sbo_id))}}"><i class="fa fa-fw fa-close"  title="Delete"></i></a></td>
									</tr>
									<?php
									}
									?>
							
									
								</tbody>
								
							</table>
							<?php echo $data->render(); ?>
						</div>
					</div>
				   
				</div>
			</div>
			<!-- /.box-body -->
		</div>


		
		  
          <!-- /.box -->
        
	</div>

@endsection
