@extends('adminlte::page')

@section('title', 'Options')

@section('content_header')
    <h1>Edit Page</h1>
@stop




@section('content')


	  <!-- Include Editor JS files. -->
	<div class="col-md-12">
        @include('layouts.alert')
        <div class="panel panel-default">
			<div class="panel-body">
				<form method="POST" id="addShops" action="{{ url('/cp-admin/home_box_update/') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data" method="post">
				
				
				
					{{ csrf_field() }}
					<div class="box-body">
						<div class="form-group">
							<label for="inputText1" class="col-sm-2 control-label">Title</label>

							<div class="col-sm-10">
								<input type="text" class="form-control" id="hbox_title" name="hbox_title" placeholder="Title" value="{{$data->hbox_title}}">
								{!! $errors->first('hbox_title', '<p class="help-block">:message</p>') !!}
							
							</div>
						</div>
						
						
						<div class="form-group">
							<label for="inputText1" class="col-sm-2 control-label">Image</label>

							<div class="col-sm-10">
								<input type="file" class="form-control" id="hbox_image" name="hbox_image">
								{!! $errors->first('hbox_image', '<p class="help-block">:message</p>') !!}
								
								<input type="hidden" name="old_hbox_image" id="old_hbox_image" value="{{$data->hbox_image}}" />
							
							</div>
						</div>
						
						
						
						
						
					
						
						
						
					</div>
					
					<input type="hidden" name="hbox_id" id="hbox_id" value="{{$data->hbox_id}}" />
					
					  <!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-default">Cancel</button>
						<button type="submit" class="btn btn-info pull-right">Update</button>
					</div>
					  <!-- /.box-footer -->
				
				</form>
				
				
			</div>
          <!-- /.box -->
        
		</div>
       
      
	</div>
      <!-- /.row -->
	  <!-- Include Editor style. -->
	  

    
	
@endsection


