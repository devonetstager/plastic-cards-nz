@extends('adminlte::page')

@section('title', 'Home Page - Our Clients')

@section('content_header')
    <h1> Home Page - Our Clients </h1>
@stop

@section('content')

    <div class="col-md-12">
        @include('layouts.alert')
		
		<div class="box">
			<div class="box-header">
				<div class="row">
					<div class="col-sm-10"><h3 class="box-title">Home Page - Our Clients</h3></div>
					<div class="col-sm-2"><a href="{{ url('/cp-admin/home_clients_add/') }}" class="btn btn-block btn-primary">Add New</a></div>
				</div>
				
				
				
			</div>
			
			<!-- /.box-header -->
			<div class="box-body">
				<div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
					<div class="row">
						<div class="col-sm-6"></div>
						<div class="col-sm-6"></div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
								<thead>
									<tr role="row" class="odd">
										<td class="sorting_1">No</td>
										<td>Heading</td>
										<td>Image</td>
										
										<td>&nbsp;</td>
									</tr>
								</thead>
								<tbody id="sortable">
									<?php
									$page_no = 0;
									foreach($data as $details)
									{
										$page_no++;
									?>
									<tr role="row" class="odd" id="item-{{$details->clients_id}}">
										<td><?php echo $page_no;?></td>
										<td>
										<?php 
										
										echo $details->clients_title;
										?>
										</td>	
										<td>
											@if($details->clients_image!="")
												
											<img src="{{asset('public/homeassets/'.$details->clients_image)}}" width="100" />
											@endif
										</td>	
										<td>
										<a href="{{URL::route('home_clients_edit',Crypt::encrypt($details->clients_id))}}"><i class="fa fa-pencil-square-o"  title="Edit"></i></a>
										
										<a href="{{URL::route('home_clients_delete',Crypt::encrypt($details->clients_id))}}"><i class="fa fa-fw fa-close"  title="Delete"></i></a>
										
										</td>
									</tr>
									<?php
									}
									?>
							
									
								</tbody>
								
							</table>
						</div>
					</div>
				   
				</div>
			</div>
			<!-- /.box-body -->
		</div>


		
		  
          <!-- /.box -->
        
	</div>
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>	
	<script>
  $( function() {
    $( "#sortable" ).sortable({
		axis: 'y',
		stop: function (event, ui) {
			var data = $(this).sortable('serialize');
			//alert(data);

			// POST to server using $.post or $.ajax
			$.ajax({
				_token: "{{ csrf_token() }}",
				data: data,
				type: 'get',
				url: "{{route('sort_order')}}"
			});
		}
		
	});
    $( "#sortable" ).disableSelection();
  } );
  </script>

@endsection
