@extends('adminlte::page')

@section('title', 'Options')

@section('content_header')
    <h1>Pages</h1>
@stop

@section('content')

    <div class="col-md-12">
        @include('layouts.alert')
		
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Pages</h3>
			</div>
			
			<!-- /.box-header -->
			<div class="box-body">
				<div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
					<div class="row">
						<div class="col-sm-6"></div>
						<div class="col-sm-6"></div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
								<thead>
									<tr role="row" class="odd">
										<td class="sorting_1">No</td>
										<td>Heading</td>
										
										<td>&nbsp;</td>
									</tr>
								</thead>
								<tbody>
									<?php
									$page_no = 0;
									foreach($page_list as $details)
									{
										$page_no++;
									?>
									<tr role="row" class="odd">
										<td><?php echo $page_no;?></td>
										<td>
										<?php 
										if($details['page_parent']!=0){
											echo "&nbsp;&nbsp;&nbsp;---";
										}
										echo $details['page_heading'];
										?>
										</td>				
										<td>
										<a href="{{URL::route('edit_page',Crypt::encrypt($details->id))}}"><i class="fa fa-pencil-square-o"  title="Edit"></i></a>										
										</td>
									</tr>
									<?php
									}
									?>
									<!--<tr role="row" class="even">
										<td>Gecko</td>
										<td>Firefox 1.5</td>
										<td>A</td>
									</tr>-->
									
								</tbody>
								
							</table>
						</div>
					</div>
				   
				</div>
			</div>
			<!-- /.box-body -->
		</div>


		
		  
          <!-- /.box -->
        
	</div>

@endsection
