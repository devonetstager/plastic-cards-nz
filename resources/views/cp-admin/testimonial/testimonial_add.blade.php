@extends('adminlte::page')

@section('title', 'Add New - Testimonial')

@section('content_header')
    <h1>Add New - Testimonial</h1>
@stop




@section('content')


	  <!-- Include Editor JS files. -->
	<div class="col-md-12">
        @include('layouts.alert')
        <div class="panel panel-default">
			<div class="panel-body">
				<form method="POST" id="addShops" action="{{ url('/cp-admin/testimonial_save/') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data" method="post">
				
				
				
					{{ csrf_field() }}
					<div class="box-body">
						<div class="form-group">
							<label for="inputText1" class="col-sm-2 control-label">Title</label>

							<div class="col-sm-10">
								<input type="text" class="form-control" id="tes_title" name="tes_title" placeholder="Title" value="">
								{!! $errors->first('tes_title', '<p class="help-block">:message</p>') !!}
							
							</div>
						</div>
						
						
						<div class="form-group">
							<label for="inputText1" class="col-sm-2 control-label">Testimonial</label>

							<div class="col-sm-10">
								<textarea class="form-control" id="tes_description" name="tes_description"></textarea>
								{!! $errors->first('tes_description', '<p class="help-block">:message</p>') !!}
								
								
							
							</div>
						</div>
						
						
						
						
						
					
						
						
						
					</div>
					
			
					
					  <!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-default">Cancel</button>
						<button type="submit" class="btn btn-info pull-right">Update</button>
					</div>
					  <!-- /.box-footer -->
				
				</form>
				
				
			</div>
          <!-- /.box -->
        
		</div>
       
      
	</div>
      <!-- /.row -->
	  <!-- Include Editor style. -->
	  

    
	
@endsection


