@extends('frontend.Layouts.app')
@section('content')
<section class="sec1 txt" style="background-image: url({{asset('public/homeassets/'.$data->home_banner_image)}})">
    <div class="wid">
        <div class="logoText"><a href="{{ url('/') }}" title="Custom Plastic Card"><img src="{{asset('public/frontend/images/customcardlogo.png')}}" alt="Custom Plastic Card" title="Custom Plastic Card"></a></div>
        <h1><?php echo $data->home_banner_title;?></h1>
        <a href="{{ url('/quote.htm/') }}" title="Get A Quote">Get A Quote</a>
    </div>
</section>
<section class="sec2">
    <div class="wid">
	<?php
	$home_banner_features	= $data->home_banner_features;
	$home_banner_features_arr	= explode(",",$home_banner_features);
	if(count($home_banner_features_arr)>0)
	{
	?>
        <ul>
			<?php 
			foreach($home_banner_features_arr as $val)
			{
				?>
            <li><span class="hmeSpn"></span>{{$val}}</li>
			<?php
			}
			?>
          
        </ul>
	<?php
	}
	?>
    </div>
</section>
<section class="sec3">
    <div class="wid">
        <h2>{{$data->home_heading}}</h2>
		<?php echo $data->home_description;?>
        
        <div class="fullWdth">
			<?php
			if(count($hbox)>0)
			{
				$bx_cnt = 0;
				foreach($hbox as $hboxdata)
				{
					$bx_cnt++;
				?>
            <div class="hoemFbox">
				@if($hboxdata->hbox_image!="")
				<div class="hmbx-img"><img src="{{asset('public/homeassets/'.$hboxdata->hbox_image)}}" alt="<?php echo str_replace('<br />',' ',$hboxdata->hbox_title);?>" title="<?php echo str_replace('<br />',' ',$hboxdata->hbox_title);?>"></div>							
				
				@endif
											
                
                <h3 class="hbxe_{{$bx_cnt}}"><?php echo $hboxdata->hbox_title;?></h3>
            </div>
			<?php
				}
			}
			?>
            
        </div>
    </div>
</section>
<section class="sec4">
    <div class="wid">
        <div class="co4Left">
            <h2><?php echo $data->home_section2_title;?></h2>
            <?php echo $data->home_section2_description;?>
            <a href="{{ url('/gallery.html/') }}" title="View Our Gallery">View Our Gallery</a>
        </div>
        <div class="co4Right">
		@if($data->home_section2_image!="")
		<img src="{{asset('public/homeassets/'.$data->home_section2_image)}}" alt="{{$data->home_section2_title}}" title="{{$data->home_section2_title}}">					
		
		@endif		
		</div>
    </div>
</section>
<section class="sec5">
    <div class="wid">
        
        <div class="secTestmonial">
            <h2><?php echo $data->home_testimonial_title;?></h2>
			<div class="testSubhead">Just a small sample of our Google reviews</div>
            <div class="testCovr">
				<ul>
					<?php
					if(count($testimonials)>0)
					{
						foreach($testimonials as $testimonials_item)
						{
						?>
						<li>
							<?php echo $testimonials_item->tes_description;?>
							<small>- <?php echo $testimonials_item->tes_title;?></small>
							
							
					
							<div class="star"><img src="{{asset('public/frontend/images/star.png')}}" alt="Star" title="Star"></div>
						</li>
						
						
						<?php
						}
					}
					?>
					
				</ul>	
                
				
				<?php //echo $data->home_testimonial_description;?>
                
            </div>
        </div>
		
		
    </div>
</section>
<section class="sec6">
    <div class="wid">
        <div class="clCont"><h3>Some of Our Clients</h3></div>
        <div class="clitS">
			<?php 
			if(count($hclients)>0) 
			{ 
				foreach($hclients as $hclientsdata)
				{
			?>
		
			<img src="{{asset('public/homeassets/'.$hclientsdata->clients_image)}}" alt="{{$hclientsdata->clients_title}}" title="{{$hclientsdata->clients_title}}">
			<?php 
				} 
			}
			?>
		</div>
    </div>
</section>

@endsection