@extends('frontend.Layouts.app')
@section('content')
<div class="innerpage page_{{$data->id}}">
	<div class="innerbanner">
		<div class="wid">
			<h1><?php echo $data->page_heading;?></h1>
		</div>
	</div>
	<div class="wid">
		<div class="contLft">
			<div class="innerCvr">
                <?php echo $data->page_description;?> 
				
				<?php
				if(count($galleryPages)>0)
				{
					foreach($galleryPages as $gpage)
					{
					    if($gpage->page_id==11||$gpage->page_id==12||$gpage->page_id==13||$gpage->page_id==24)
					    {
					        continue;
					        
					    }
					?>
					<div class="fullWdth galleryMain">
						<?php
						if($gpage->page_image!="") { 
						?>
						<div class="galleryLeft"><img src="{{asset('public/pageassets/'.$gpage->page_image)}}" alt="<?php echo $gpage->page_heading;?>" title="<?php echo $gpage->page_heading;?>" /></div>
						<?php
						}
						?>
						<div class="galleryRight">
							<div class="glryTitl"><?php echo $gpage->page_heading;?></div>
							<p>
							<?php 
								$pgDescription = strip_tags($gpage->page_description);
								echo excerpt($pgDescription,30);
								
							
							?>
							</p>
							<a href='{{ url("/$gpage->slug_url/") }}' title="<?php echo $gpage->page_heading;?>" class="galink">View More</a>
						</div>
					</div>
					<?php
					}
				}
				?>
				
			</div>
		</div>
	</div>
</div>
<?php
function excerpt($content,$limit)
{
	$excerpt = explode(' ', $content , $limit);
	if (count($excerpt) >= $limit)
	{
		array_pop($excerpt);
		$excerpt = implode(" ", $excerpt) . '...';
	}
	else
	{
		$excerpt = implode(" ", $excerpt);
	}

	$excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);
	return $excerpt;
} 
?>
@endsection