@extends('frontend.Layouts.app')
@section('content')
<div class="innerpage page_{{$data->id}}">
	<div class="innerbanner">
		<div class="wid">
			<h1><?php echo $data->page_heading;?></h1>
		</div>
	</div>
	<div class="wid">
		<?php echo $data->page_description;?>
		<div class="contLft">
			<div class="innerCvr">
				<div class="contctFull">
					<div class="contaLeft">
						<p class="contactIco PhnIco">TOLL FREE <a href="tel:<?php echo $options->phonenumber;?>" title="<?php echo $options->phonenumber;?>"><?php echo $options->phonenumber;?></a></p>
						<p class="contactIco mailIco"><a href="mailto:<?php echo $options->email;?>" title="<?php echo $options->email;?>"><?php echo $options->email;?></a></p>
						<div class="conimg">
							<img src="{{asset('public/frontend/images/contact.jpg')}}" alt="<?php echo $data->page_heading;?>" title="<?php echo $data->page_heading;?>" >
						</div>
					</div>
					<div class="contaRight">
					    <div class="submitOrdrNow">Submit order now (no payment required) </div>
						<form method="POST" id="formId" action="{{ url('/order_submit/') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data" method="post" onsubmit="return validation_check_contact();">
							{{ csrf_field() }}
							<ul>
								<li><label>First Name:</label><input title="First Name" type="text" name="first_name" id="first_name"  class="input val_req">
								<span class="hdfrm"></span></li>
								<li><label>Last Name:</label><input title="Last Name" type="text" name="last_name" id="last_name" class="input val_req">
								<span class="hdfrm"></span></li>
								<li><label>Email:</label><input title="Email Address" type="text" name="email_id" id="email_id" class="input val_req val_email">
								<span class="hdfrm"></span></li>
								<li>
									<label>Phone</label>
									<input title="Phone" type="text" name="e_phone" id="e_phone" class="input val_req val_phone">
									<span class="hdfrm"></span>
								</li>
								<li class="upldFile">
									<label>Upload File</label>
									<input title="Upload File" type="file" name="upload_file" id="upload_file" class="">
									<span class="hdfrm"></span>
								</li>
								
								<li><label>Design and order comments:</label><textarea class="inputtextar" id="comments" name="comments"></textarea><span class="hdfrm"></span></li>
							</ul>
							<input type="hidden" name="recaptcha_response" id="recaptchaResponse">
							<input type="submit" class="subbtn" value="Submit">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
<script src="https://www.google.com/recaptcha/api.js?render=6LfyFqsUAAAAANWuODhV-UhS9O71A2HiGs_9nQQc"></script>
    <script>
        grecaptcha.ready(function () {
            grecaptcha.execute('6LfyFqsUAAAAANWuODhV-UhS9O71A2HiGs_9nQQc', { action: 'contact' }).then(function (token) {
                var recaptchaResponse = document.getElementById('recaptchaResponse');
                recaptchaResponse.value = token;
            });
        });
    </script>
@endsection