@extends('frontend.Layouts.app')
@section('content')
<div class="innerpage page_{{$data->id}}">
	<div class="innerbanner">
		<div class="wid">
			<h1><?php echo $data->page_heading;?></h1>
		</div>
	</div>
	<div class="wid">
		<div class="contLft">
			<div class="innerCvr">
				<div class="contctFull">
					<?php echo $data->page_description;?>
					<em>* Required Fields</em>
					<div class="quote">
						<form method="POST" id="formId" action="{{ url('/quote_submit/') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data" method="post" onsubmit="return validation_check_contact();">
							{{ csrf_field() }}
							<ul>
								<li>
									<em class="starR">*</em>
									<label>Card Type</label>
									<select name="card_type" size="1" class="val_req">
										<option value="" selected="">Choose</option>
										<option value="Membership Card">Membership</option>
										<option value="Discount card">Discount</option>
										<option value="Identification card">Identification</option>
										<option value="Business card">Business</option>
										<option value="Gift card">Gift</option>
										<option value="Loyalty card">Loyalty</option>
										<option value="Novelty card">Novelty</option>
										<option value="Advertising card" >Advertising</option>
										<option value="Security card">Security</option>
										<option value="Fundraising card">Fundraising</option>
										<option value="Student card">Student</option>
										<option value="Key tag">Key Tag</option>
										<option value="Combo Card">Card-Key Tag Combo</option>
										<option value="Environmentally friendly - Corn Cards">Environmentally
											friendly - Corn Cards
										</option>
										<option value="Environmentally friendly - Teslin">Environmentally
											friendly - Teslin
										</option>
										<option value="Environmentally friendly - Recycled PVC">Environmentally
											friendly - Recycled PVC
										</option>
										<option value="Other card type">Other</option>
									</select>
									<span class="hdfrm"></span>
								</li>
								<li><em class="starR">*</em>
									<label>Number of cards</label>
									<input name="no_of_cards" type="text" class="input val_req val_number">
									<span class="hdfrm"></span>
								</li>
							</ul>
							<div class="fullWdth checkfield">
								<label><strong>Features</strong></label>
								<span>
								<input type="checkbox" class="input" name="features[]" value="Barcode";>
								Barcode</span><span>
								<input type="checkbox" class="input" name="features[]" value="Numbering">
								Numbering </span><span>
								<input type="checkbox" class="input" name="features[]" value="Signature Panel">
								Signature Panel </span><span>
								<input type="checkbox" class="input" name="features[]" value="Personalization">
								Personalization </span>
								<span>
								<input type="checkbox" class="input" name="features[]" value="Scratch Off">
								Scratch Off </span>
								<span>
								<input type="checkbox" class="input" name="features[]" value="Embossing (raised print)">
								Embossing (raised print) </span>
							</div>
							<ul>
								<li>
									<label>Card Thickness<span> (0.76 mm standard credit card thickness)</span></label>
									<select name="cardthickness" size="1">
										<option value="0.76 mm thick">0.76 mm</option>
										<option value="0.58 mm thick">0.58 mm</option>
										<option value="0.38 mm thick">0.38 mm</option>
										<option value="0.28 mm thick">0.28 mm</option>
									</select>
								</li>
								<li>
									<em class="starR">*</em>
									<label> Numbers of colours on the front</label>
									<select name="no_of_colors_front" size="1" class="val_req">
										<option value="" selected="">Choose</option>
										<option value="Black Only">Black Only</option>
										<option value="1 Color">1 Color</option>
										<option value="2 Colors">2 Colors</option>
										<option value="3 Colors">3 Colors</option>
										<option value="4 Colors">4 Colors</option>
										<option value="None">None</option>
									</select>
									<span class="hdfrm"></span>
								</li>
								<li>
									<em class="starR">*</em>
									<label> Numbers of colours on the back</label>
									<select name="no_of_colors_back" size="1" class="val_req">
										<option value="" selected="">Choose</option>
										<option value="Black Only">Black Only</option>
										<option value="1 Color">1 Color</option>
										<option value="2 Colors">2 Colors</option>
										<option value="3 Colors">3 Colors</option>
										<option value="4 Colors">4 Colors</option>
										<option value="None">None</option>
									</select>
									<span class="hdfrm"></span>
								</li>
								<li>
									<label> Magnetic Stripe</label>
									<select name="magnetic_stripe">
										<option value="None" selected="">None</option>
										<option value="Hi Coercivity">HiCo</option>
										<option value="Low Coercivity">LoCo</option>
									</select>
								</li>
								<li>
									<label> Date card required</label>
									<input type="text" class="input" name="date_card" id="datepicker" readonly>
								</li>
								<li>
									<label> Do you require fulfillment?<span>(backing, packaging, mass mailing etc.):</span></label>
									<select name="fullfillment">
										<option value="Yes">Yes</option>
										<option value="No" selected="">No</option>
									</select>
								</li>
								<li>
									<label>How did you find us? <span> (enter search engine):</span></label>
									<input type="text" class="input" name="how_findus">
								</li>
							</ul>
							<h2>CUSTOMER INFORMATION:</h2>
							<ul>
								<li><em class="starR">*</em><label>First Name:</label><input type="text" class="input val_req" name="firstname"><span class="hdfrm"></span></li>
								<li><label>Last Name:</label><input type="text" class="input" name="lastname"></li>
								<li><em class="starR">*</em><label>Email:</label><input type="text" class="input val_req val_email"  name="c_email"><span class="hdfrm"></span></li>
								<li><label>Phone:</label><input type="text" class="input  val_phone" name="c_phone"><span class="hdfrm"></span></li>
								<li class="onecol"><label>Comments and Additional Requirements:</label><textarea class="inputtextar" name="comments"></textarea></li>
							</ul>
							<input type="hidden" name="recaptcha_response" id="recaptchaResponse">
							<input type="submit" class="subbtn" value="Submit">
						</form>
					</div>
				</div>

				
			</div>
		</div>
	</div>
</div>
<script src="https://www.google.com/recaptcha/api.js?render=6LfyFqsUAAAAANWuODhV-UhS9O71A2HiGs_9nQQc"></script>
    <script>
        grecaptcha.ready(function () {
            grecaptcha.execute('6LfyFqsUAAAAANWuODhV-UhS9O71A2HiGs_9nQQc', { action: 'contact' }).then(function (token) {
                var recaptchaResponse = document.getElementById('recaptchaResponse');
                recaptchaResponse.value = token;
            });
        });
    </script>
@endsection