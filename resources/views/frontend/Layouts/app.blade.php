<!DOCTYPE html>
<html lang="en-AU">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title><?php echo $data->meta_title;?></title>
		<meta name="description"  content="<?php echo $data->meta_description;?>" />
		<meta name="keywords"  content="<?php echo $data->meta_keywords;?>" />

        <link rel="icon" type="image/png" sizes="32x32" href="{{asset('public/frontend/images/favicon.png')}}" />
        <link rel="icon" type="image/png" sizes="32x32" href="{{asset('public/frontend/images/favicon-32x32.png')}}">
        <link rel="icon" type="image/png" sizes="96x96" href="{{asset('public/frontend/images/favicon-96x96.png')}}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{asset('public/frontend/images/favicon-16x16.png')}}">
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('public/frontend/images/fav-icon.png')}}">
        <link href="{{asset('public/frontend/css/style.css')}}" rel="stylesheet" type="text/css">
        <link rel="canonical" href="{{ url(Request::url()) }}" />
        
        <?php if($data->id!=22) { ?>
        <!-- Global site tag (gtag.js) - Google Ads: 969430363 --> 
	<script async src="https://www.googletagmanager.com/gtag/js?id=AW-969430363"></script> 

	<script> 
	window.dataLayer = window.dataLayer || []; 
	function gtag(){dataLayer.push(arguments);} 
	gtag('js', new Date()); 
	
	gtag('config', 'AW-969430363'); 
	</script> 
	
	<script> 
	gtag('config', 'AW-969430363/xAGHCJWdglYQ26qhzgM', { 
	'phone_conversion_number': '0800 022 737' 
	}); 
	</script> 
	<!-- Event snippet for Lead conversion page --> 

        
         <?php
        }
        ?>
        <?php if($data->id==22) { ?>
        
        <!-- Global site tag (gtag.js) - Google Ads: 969430363 --> 
	<script async src="https://www.googletagmanager.com/gtag/js?id=AW-969430363"></script> 

	<script> 
	window.dataLayer = window.dataLayer || []; 
	function gtag(){dataLayer.push(arguments);} 
	gtag('js', new Date()); 
	
	gtag('config', 'AW-969430363'); 
	</script> 
	
	<script> 
	gtag('config', 'AW-969430363/xAGHCJWdglYQ26qhzgM', { 
	'phone_conversion_number': '0800 022 737' 
	}); 
	</script> 
	<!-- Event snippet for Lead conversion page --> 
	<script> 
	gtag('event', 'conversion', { 
	'send_to': 'AW-969430363/mMfNCJ2B3gIQ26qhzgM', 
	'value': 1.0, 
	'currency': 'AUD' 
	}); 
	</script> 
	
	<script src="https://www.google-analytics.com/urchin.js"></script>

<script src="//www.googleadservices.com/pagead/conversion.js"></script>
        
        
        <?php
        }
        ?>
        
    </head>
    <body>
        <header>
            <div class="wid">
                <div class="mobtxt"><a href="{{ url('/') }}" title="Custom Platic Card"><img title="Custom Platic Card" alt="Custom Platic Card" src="{{asset('public/frontend/images/plastic-cards-newzealand.png')}}"></a></div>
                <div class="flt-left">
                    <div class="menuD">
                        <a href="#menu" id="nav-toggle" class="menu-link"><span></span></a>
                        <nav id="menu" class="menu">
                            <ul class="level-01">
                                <li class="nav-current"><a href="{{ url('/') }}" title="Home">Home</a></li>
                                <li class="menu-item-has-children">
                                    <a href="{{ url('/about-us.html/') }}" title="About Us">About Us</a>
                                    <ul class="sub-menu">
                                        <li><a href="{{ url('/contactus.html/') }}" title="Contact Us">Contact Us </a></li>
                                        <li><a href="{{ url('/testimonials.html/') }}" title="Testimonials">Testimonials</a></li>
                                        <li><a href="{{ url('/warranty.html/') }}" title="Generous Warranty">Generous Warranty</a></li>
                                    </ul>
                                </li>
                                <li><a href="{{ url('/services.html/') }}" title="Services">Services</a></li>
                                <li><a href="{{ url('/quote.htm/') }}" title="Quote">Quote</a></li>
                                <li class="menu-item-has-children">
                                    <a href="{{ url('/gallery.html/') }}" title="Gallery">Gallery</a>
                                    <ul class="sub-menu">
                                        <li><a href="{{ url('/ccardsize.html/') }}" title="Credit Card Size">Credit Card Size</a></li>
										<li><a href="{{ url('/keytags.html/') }}" title="Key Tags">Key Tags</a></li>
										<!--<li><a href="{{ url('/businesscards.html/') }}" title="Business Cards">Business Cards</a></li>-->
										<li><a href="{{ url('/combos.html/') }}" title="Combo Cards">Combo Cards</a></li>
										<!--<li><a href="{{ url('/idcards.html/') }}" title="ID Cards">ID Cards</a></li>
										<li><a href="{{ url('/discountcards.html/') }}" title="Discount Cards">Discount Cards</a></li>
										<li><a href="{{ url('/membershipcards.html/') }}" title="Membership Cards">Membership Cards</a></li>-->
										<li><a href="{{ url('/customcards.html/') }}" title="Custom Cards">Custom Cards</a></li>
										<li><a href="{{ url('/mailers.html/') }}" title="Mailers">Mailers</a></li>
										<li><a href="{{ url('/ordersamples.html/') }}" title="Order Samples">Order Samples</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="{{ url('/specs.htm/') }}" title="Specs">Specs</a>
                                    <ul class="sub-menu">
                                        <li><a href="{{ url('/templates.html/') }}" title="Templates">Templates</a></li>
                                        <li><a href="{{ url('/designservices.html/') }}" title="Design Services">Design Services</a></li>
                                        <li><a href="{{ url('/submission.asp/') }}" title="Submit Order">Submit Order</a></li>
                                    </ul>
                                </li>
                                <li><a href="{{url('/faqs.html/')}}" title="FAQ’s">FAQ’s</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="flt-right"><a href="tel:<?php echo str_replace(' ', '', $options->phonenumber);?>" class="hmTel" title="<?php echo $options->phonenumber;?>"><?php echo $options->phonenumber;?></a></div>
            </div>
			<section class="mobBtns">
				<ul>
					<li class="mobCal"><a href="tel:<?php echo str_replace(' ', '', $options->phonenumber);?>" class="hmTel" title="<?php echo $options->phonenumber;?>"><span></span><?php echo $options->phonenumber;?></a></li>
					<li class="mobQte"><a href="{{ url('/quote.htm/') }}" title="Get A Quote">Get A Quote<span></span></a></li>
				</ul>
			</section>
        </header>
		
		@yield('content')
		
		<footer>
            <div class="wid">
                <div class="footbx1 flt-left">
                    <div class="ftrLgo"><a href="{{ url('/') }}" title="Custom Platic Card"><img title="Custom Platic Card" alt="Custom Platic Card" src="{{asset('public/frontend/images/plastic-cards-newzealand-footer-logo.png')}}"></a>
					</div>
                    <p>Copyright <?php echo date("Y");?> <a target="_blank" href="https://www.clixpert.com.au/" rel="nofollow">Marketing<br>
                        Agency Sydney.</a>  All Rights Reserved.
                    </p>
                </div>
                <div class="footbx2 flt-left">
                    <p><span>Phone</span><br>
                        TOLL FREE <a href="tel:<?php echo str_replace(' ', '', $options->phonenumber);?>" title="<?php echo $options->phonenumber;?>"><?php echo $options->phonenumber;?></a>
                    </p>
                    <p> <strong class="ftmail">Email</strong><br>
                        <a href="mailto:<?php echo $options->email;?>" title="<?php echo $options->email;?>"><?php echo $options->email;?></a>
                    </p>
                    <p><span>Location</span><br>
                        <?php echo $options->address;?>
                    </p>
                </div>
                <div class="footbx3 flt-left">
                    <h4>Home</h4>
                    <p> <a href="{{ url('/about-us.html/') }}" title="About Us">About Us</a><br>
                        <a href="{{ url('/services.html/') }}" title="Services">Services</a><br>
                        <a href="{{ url('/quote.htm/') }}" title="Quote">Quote</a><br>
                        <a href="{{ url('/gallery.html/') }}" title="Gallery">Gallery</a><br>
                        <a href="{{ url('/specs.htm/') }}" title="Specs">Specs</a><br>
                        <a href="{{ url('/faqs.html/') }}" title="FAQ’s">FAQ’s</a><br>
                        <a href="{{ url('/sitemap.html/') }}" title="Sitemap">Sitemap</a><br>
                        <a href="{{ url('/privacy.html/') }}" title="Privacy">Privacy</a>
                    </p>
                </div>
                <div class="footbx4 flt-left">
                    <h4>We Ship Orders By</h4>
                    <img src="{{asset('public/frontend/images/plastic-cards-newzealand-couriers.png')}}" alt="Newzealand Couriers" title="Newzealand Couriers">
                </div>
                <div class="footbx5 flt-right">
                    <h4>We Accept</h4>
                    <img src="{{asset('public/frontend/images/plastic-cards-newzealand-visa.png')}}" alt="We Accept" title="We Accept">
                </div>
            </div>
        </footer>
        <script src="{{asset('public/frontend/js/jquery.min.js')}}"></script> 
        <script src="{{asset('public/frontend/js/script.js')}}" ></script> 
        <script>
            document.querySelector( "#nav-toggle" ).addEventListener( "click", function() {
            this.classList.toggle( ".active" );
            });
        </script> 
        <script src="{{asset('public/frontend/js/classie.js')}}"></script> 
        <script>
            function init() {
                window.addEventListener('scroll', function(e){
                    var distanceY = window.pageYOffset || document.documentElement.scrollTop,
                        shrinkOn = 110,
                        header = document.querySelector("header");
                    if (distanceY > shrinkOn) {
                        classie.add(header,"smaller");
                    } else {
                        if (classie.has(header,"smaller")) {
                            classie.remove(header,"smaller");
                        }
                    }
                });
            }
            window.onload = init();
        </script>

<?php if($data->id==6) { ?>		
<link href="{{asset('public/frontend/datepicker/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('public/frontend/datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css">  
  <script src="{{asset('public/frontend/datepicker/bootstrap-datepicker.min.js')}}"></script>
<script>
  $( function() {
    $( "#datepicker" ).datepicker({
		format: 'dd-mm-yyyy',
		autoclose: true
	});
  } );
  </script>
  
<?php } ?>  
  
  
    </body>
</html>	