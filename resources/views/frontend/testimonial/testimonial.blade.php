@extends('frontend.Layouts.app')
@section('content')
<div class="innerpage page_{{$data->id}}">
	<div class="innerbanner">
		<div class="wid">
			<h1><?php echo $data->page_heading;?></h1>
		</div>
	</div>
	<div class="wid">
		<div class="contLft">
			<div class="innerCvr">
                <?php echo $data->page_description;?>
				
				<?php
				if(count($testimonials)>0)
				{
					?>
					<h2>Here are some recent and genuine comments by our clients:</h2>
					<?php
					foreach($testimonials as $testimonials_item)
					{
					
				?>
				
				
						<p><?php echo $testimonials_item->tes_description;?><br>
						<span class="oreBld"><?php echo $testimonials_item->tes_title;?></span></p>
				<?php
					}
				}
				?>
				
				
			</div>
		</div>
	</div>
</div>
@endsection