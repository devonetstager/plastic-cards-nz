@extends('frontend.Layouts.app')
@section('content')
<div class="innerpage page_{{$data->id}}">
	<div class="innerbanner">
		<div class="wid">
			<h1><?php echo $data->page_heading;?></h1>
		</div>
	</div>
	<div class="wid">
		<div class="contLft">
			<div class="innerCvr">
                <?php echo $data->page_description;?>
				
				<div class="contctFull">
					<div class="contaLeft">
						<p class="contactIco PhnIco">TOLL FREE <a href="tel:<?php echo $options->phonenumber;?>" title="<?php echo $options->phonenumber;?>"><?php echo $options->phonenumber;?></a>
						
						  
						</p>
						<p class="contactIco PhnIco">Local number <a href="tel:099518007" title="099518007">099518007</a>
						
						  
						</p>
						<p class="contactIco mailIco"><a href="mailto:<?php echo $options->email;?>" title="<?php echo $options->email;?>"><?php echo $options->email;?></a></p>
						<p class="contactIco addreIco"><?php echo strip_tags($options->address);?></p>
						<div class="conimg">
							<img src="{{asset('public/frontend/images/contact.jpg')}}" alt="<?php echo $data->page_heading;?>" title="<?php echo $data->page_heading;?>" >
						</div>
					</div>
					<div class="contaRight">
						<form method="POST" id="formId" action="{{ url('/contact_submit/') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data" method="post" onsubmit="return validation_check_contact();">
							{{ csrf_field() }}
							<ul>
								<li><label>First Name:</label><input title="First Name" type="text" name="first_name" id="first_name"  class="input val_req"><span class="hdfrm"></span></li>
								<li><label>Last Name:</label><input title="Last Name" type="text" name="last_name" id="last_name" class="input val_req"><span class="hdfrm"></span></li>
								<li><label>Email:</label><input title="Email Address" type="text" name="email_id" id="email_id" class="input val_req val_email"><span class="hdfrm"></span></li>
								<li>
									<label>Inquiry Type</label>
									<select name="inquiry_type" id="inquiry_type" class="input">
										<option value="">Inquiry Type</option>
										<option value="Sales">Sales</option>
										<option value="Inquiry">Inquiry</option>
										<option value="General Comments">General
											Comments
										</option>
										<option value="Other">Other</option>
									</select><span class="hdfrm"></span>
								</li>
								<li><label>Comments and Additional Requirements:</label><textarea class="inputtextar" id="comments_requirement" name="comments_requirement"></textarea><span class="hdfrm"></span></li>
							</ul>
							<input type="hidden" name="recaptcha_response" id="recaptchaResponse">
							<input type="submit" class="subbtn" value="Submit">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="https://www.google.com/recaptcha/api.js?render=6LfyFqsUAAAAANWuODhV-UhS9O71A2HiGs_9nQQc"></script>
    <script>
        grecaptcha.ready(function () {
            grecaptcha.execute('6LfyFqsUAAAAANWuODhV-UhS9O71A2HiGs_9nQQc', { action: 'contact' }).then(function (token) {
                var recaptchaResponse = document.getElementById('recaptchaResponse');
                recaptchaResponse.value = token;
            });
        });
    </script>
@endsection