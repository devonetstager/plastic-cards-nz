@extends('frontend.Layouts.app')
@section('content')
<div class="innerpage page_{{$data->id}}">
	<div class="innerbanner">
		<div class="wid">
			<h1><?php echo $data->page_heading;?></h1>
		</div>
	</div>
	<div class="wid">
		<div class="contLft">
			<div class="innerCvr">
				<?php echo $data->page_description;?>
				<div class="contctFull">
					<div class="quote">
						<form method="POST" id="formId" action="{{ url('/order_sample_submit/') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data" method="post" onsubmit="return validation_check_contact();">
							{{ csrf_field() }}
							<ul>
								<li>
									<em class="starR">*</em>
									<label>First Name</span></label>
									<input type="text" class="input val_req" name="firstname">
									<span class="hdfrm"></span>
								</li>
								<li><em class="starR">*</em>
									<label> Last Name</label>
									<input type="text" class="input val_req" name="lastname">
									<span class="hdfrm"></span>
								</li>
								<li><em class="starR">*</em>
									<label> Company</label>
									<input type="text" class="input val_req" name="company">
									<span class="hdfrm"></span>
								</li>
								<li>
									<label>Address1:</label>
									<input type="text" class="input" name="address1">
								</li>
								<li>
									<label> Address2:</label>
									<input type="text" class="input" name="address2">
								</li>
								<li>
									<label>City</label>
									<input type="text" class="input" name="city">
								</li>
								<li>
									<label>State/Province</label>
									<input type="text" class="input" name="state">
								</li>
								<li>
									<label>Postal Code/ZIP</label>
									<input type="text" class="input" name="zip">
								</li>
								<li>
									<label>Country</label>
									<input type="text" class="input" name="country">
								</li>
								<li>
									<label>Email</label>
									<input type="text" class="input" name="email">
								</li>
								<li>
									<label>Phone</label>
									<input type="text" class="input" name="phone">
								</li>								
								<li>
									<label>How did you find us?</label>
									<select name="how_findus" size="1" >
										<option value="">-- Select One -- </option>
										<option value="Yahoo">Yahoo</option>
										<option value="Google">Google</option>
										<option value="AOL">AOL</option>
										<option value="MSN">MSN</option>
										<option value="Mail">Mail</option>
										<option value="Email">Email</option>
										<option value="Friends">Friends</option>
										<option value="Forums">Forums</option>
										<option value="Other">Other</option>
									</select>
								</li>
								<li class="lastCmnt">
									<label>Card Type Suggestions and Comments <span> (What kind of samples you would like to see.)</span></label>
									<textarea class="input" name="comments"></textarea>
								</li>
							</ul>
							<input type="hidden" name="recaptcha_response" id="recaptchaResponse">
							<div class="btnsTwo"><input type="submit" class="subbtn" value="Submit"> <input type="reset" class="subbtn" value="Reset"></div>
						</form>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
<script src="https://www.google.com/recaptcha/api.js?render=6LfyFqsUAAAAANWuODhV-UhS9O71A2HiGs_9nQQc"></script>
    <script>
        grecaptcha.ready(function () {
            grecaptcha.execute('6LfyFqsUAAAAANWuODhV-UhS9O71A2HiGs_9nQQc', { action: 'contact' }).then(function (token) {
                var recaptchaResponse = document.getElementById('recaptchaResponse');
                recaptchaResponse.value = token;
            });
        });
    </script>
@endsection