<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class QuoteAutoResponder extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $content ;
	
    public function __construct($data)
    {
        $this->content = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
         $bcc = ['qacheck17@gmail.com'];
         return $this->view('emails.quoteautoresponder')->cc('stephanie@plastic-cards.com.au')->bcc($bcc)->subject('Quote - Plastic Cards NZ')->with('content',$this->content);    
    }
}
