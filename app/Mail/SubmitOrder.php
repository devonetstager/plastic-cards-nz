<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SubmitOrder extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $content ;
	
    public function __construct($data)
    {
        $this->content = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
         $bcc = ['mans446@hotmail.com','qacheck17@gmail.com'];
         return $this->view('emails.submitorder')->cc('stephanie@plastic-cards.com.au')->bcc($bcc)->replyTo($this->content['email_id'])->subject('Order Submission - Plastic Cards NZ')->with('content',$this->content);  
    }
}
