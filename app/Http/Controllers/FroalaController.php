<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use View;
use Image;
use Redirect;
use Crypt;

class FroalaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
   
	public function froala_upload()
    {
		
        // files storage folder
		
		$dir = public_path().'/mediauploads/';
		$_FILES['file']['type'] = strtolower($_FILES['file']['type']);
		if ($_FILES['file']['type'] == 'image/png' || $_FILES['file']['type'] == 'image/jpg' || $_FILES['file']['type'] == 'image/gif' || $_FILES['file']['type'] == 'image/jpeg' || $_FILES['file']['type'] == 'image/pjpeg')
		{	
			// setting file's mysterious name
			$filename = md5(date('YmdHis')).'.jpg';
			$file = $dir.$filename;
			// copying
			copy($_FILES['file']['tmp_name'], $file);
			// displaying file    
			$array = array(
				'link' => asset("public/mediauploads/$filename")
				
			);
			echo stripslashes(json_encode($array));   
		}
    }
	
}
