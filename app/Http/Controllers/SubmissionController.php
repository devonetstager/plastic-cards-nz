<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use View;
use Image;
use Redirect;
use Crypt;

class SubmissionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    
	public function contact_submissions()
	{
		
		$data = DB::table('contact_submissions')
				->orderBy('con_id','desc')  
				->paginate(10);
		return View::make('cp-admin.submission.contact_list')->with([
			'data'=>$data
		])->render();       
    }
	
	public function contact_submissions_delete($id,Request $request)
	{
		$id = Crypt::decrypt($id);		
		$data = DB::table('contact_submissions')
				->where('con_id',$id)       
				->delete();
        if($data)
            $request->session()->flash('success', 'Submission deleted successfully');
        else
            $request->session()->flash('info', "Can't updated  , try later.");
        return Redirect::back();
    }
	
	
	public function quote_submissions()
	{
		
		$data = DB::table('quotation_submission')
				->orderBy('qt_id','desc')  
				->paginate(10);
		return View::make('cp-admin.submission.quotation_list')->with([
			'data'=>$data
		])->render();       
    }
	
	public function quote_submissions_delete($id,Request $request)
	{
		$id = Crypt::decrypt($id);		
		$data = DB::table('quotation_submission')
				->where('qt_id',$id)       
				->delete();
        if($data)
            $request->session()->flash('success', 'Submission deleted successfully');
        else
            $request->session()->flash('info', "Can't updated  , try later.");
        return Redirect::back();
    }
	public function order_submissions()
	{
		
		$data = DB::table('submit_order_submissions')
				->orderBy('sbo_id','desc')  
				->paginate(10);
		return View::make('cp-admin.submission.order_list')->with([
			'data'=>$data
		])->render();       
    }
	
	public function order_submissions_delete($id,Request $request)
	{
		$id = Crypt::decrypt($id);		
		$data = DB::table('submit_order_submissions')
				->where('sbo_id',$id)       
				->delete();
        if($data)
            $request->session()->flash('success', 'Submission deleted successfully');
        else
            $request->session()->flash('info', "Can't updated  , try later.");
        return Redirect::back();
    }
	public function order_sample_submissions()
	{
		
		$data = DB::table('ordersamples')
				->orderBy('ord_id','desc')  
				->paginate(10);
		return View::make('cp-admin.submission.order_sample_list')->with([
			'data'=>$data
		])->render();       
    }
	public function order_sample_submissions_delete($id,Request $request)
	{
		$id = Crypt::decrypt($id);		
		$data = DB::table('ordersamples')
				->where('ord_id',$id)       
				->delete();
        if($data)
            $request->session()->flash('success', 'Submission deleted successfully');
        else
            $request->session()->flash('info', "Can't updated  , try later.");
        return Redirect::back();
    }
}
