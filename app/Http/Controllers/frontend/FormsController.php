<?php
namespace App\Http\Controllers\frontend;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
//use Illuminate\Support\Facades\Paginator;
use View;use Mail;use Redirect;
use DB;use Auth;use Input;use Hash;use Validator;use Crypt;

use Route;
use Illuminate\Http\Response; 
use Session; use Carbon;

use Illuminate\Http\Request;

use App\Mail\ContactForm;
use App\Mail\SubmitOrder;
use App\Mail\QuoteSubmit;
use App\Mail\SampleOrder;
use App\Mail\QuoteAutoResponder;





class FormsController extends Controller
{
	public function __construct(){
		ini_set("memory_limit","500M"); 
	}
	public function recaptchaVerification($recaptcha_response){
	    
	     // Build POST request:
        $recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
        $recaptcha_secret = '6LfyFqsUAAAAAJGUYALsj9neyGjMJ0ez43arhgvo';
        //$recaptcha_response = $_POST['recaptcha_response'];
    
        // Make and decode POST request:
        $recaptcha = file_get_contents($recaptcha_url . '?secret=' . $recaptcha_secret . '&response=' . $recaptcha_response);
        $recaptcha = json_decode($recaptcha);
      // dd($recaptcha);
        // Take action based on the score returned:
        
        
        // if(isset($recaptcha->score))
        // {
        try{
            if ($recaptcha->score >= 0.5) {
                // Verified - send email
                return true;
            } else {
                // Not verified - show form error
                return false;
            }
        }catch(\Exception $ex){
            return false;
        }
        // }
        // else
        // {
            // return false;
        // }
	    
	    
	}
	public function contact_submission(Request $request) {
	
		$fname		= $request->first_name;
		$lname 			= $request->last_name;		
		$email 			= $request->email_id;
		$inquiry_type 	= $request->inquiry_type;
		$comments_requirement	= $request->comments_requirement;
		$recaptcha_response	= $request->recaptcha_response;
		
		if($this->recaptchaVerification($recaptcha_response))
        {
    		DB::table('contact_submissions')
    				->insert([					
    					'con_firstname'  => $fname,
    					'con_lastname'   => $lname,
    					'con_email'      => $email,
    					'con_inquiry_type' 	 => $inquiry_type,
    					'con_inquiry'	 => $comments_requirement		
    					]);
    
    		$to_email		= "info@plastic-cards.co.nz";
    		
    		$content = array();	
    		$content['first_name'] 		=  $fname;
    		$content['last_name'] 		=  $lname;
    		$content['email_id'] 		=  $email;		
    		$content['inquiry_type']	=  $inquiry_type;
    		$content['inquiry']	=  $comments_requirement;
    		//mail session starts here
    		//	$content = [];
    			Mail::to($to_email)->queue(new ContactForm($content));
    		//mail session ends here		
    		return Redirect::to(route('thank-you.html'))->send();	
        }
        else
    	{
    	    
    	    return Redirect::back()->withErrors(['msg', 'The Message']);
    	}
	}
	
	public function order_submit(Request $request) {
	
		$fname			= $request->first_name;
		$lname 			= $request->last_name;		
		$email 			= $request->email_id;
		$e_phone 		= $request->e_phone;
		
		$comments		= $request->comments;
		
		$recaptcha_response	= $request->recaptcha_response;
		
		if($this->recaptchaVerification($recaptcha_response))
        {
            
    		if($request->hasFile('upload_file')) 
    		{
    			$imagefile 	= $request->file('upload_file') ;
    			$extension 	= $imagefile->getClientOriginalExtension();
    			$fileName 	= uniqid().time().".".$extension;
    			$folderpath = public_path().'/formsubmission/';
    			$imagefile->move($folderpath , $fileName);
    			$file_asset	= $fileName;
    	
    		}
    		else
    		{
    			$file_asset="";
    		}
    		
    		DB::table('submit_order_submissions')
    				->insert([					
    					'sbo_firstname'  => $fname,
    					'sbo_lastname'   => $lname,
    					'sbo_email'      => $email,
    					'sbo_phone' 	 => $e_phone,
    					'sbo_upload'	 => $file_asset,
    					'sbo_comments'	 => $comments,
    					
    					]);
    
    		$to_email		= "info@plastic-cards.co.nz";
    		
    		$content = array();	
    		$content['first_name'] 		=  $fname;
    		$content['last_name'] 		=  $lname;
    		$content['email_id'] 		=  $email;		
    		$content['phone']			=  $e_phone;
    		$content['comments']		=  $comments;
    		$content['file_asset']		=  $file_asset;
    		//mail session starts here
    		//	$content = [];
    			Mail::to($to_email)->queue(new SubmitOrder($content));
    		//mail session ends here		
    		return Redirect::to(route('thank-you.html'))->send();
        }
        else
    	{
    	    
    	    return Redirect::back()->withErrors(['msg', 'The Message']);
    	}
	}
	public function quote_submit(Request $request) {
	
		$card_type		= $request->card_type;
		$no_of_cards	= $request->no_of_cards;
		
		$features_arr	= $request->features;
		if($features_arr!="")
		{
			$features		= implode(", ",$features_arr);
		}
		else
		{
			$features		= "";
		}
		

		$cardthickness		= $request->cardthickness;
		$no_of_colors_front	= $request->no_of_colors_front;
		$no_of_colors_back	= $request->no_of_colors_back;
		$magnetic_stripe	= $request->magnetic_stripe;
		$date_card			= $request->date_card;
		$how_findus			= $request->how_findus;
		$firstname			= $request->firstname;
		$lastname			= $request->lastname;
		$c_email			= $request->c_email;
		$c_phone			= $request->c_phone;		
		$comments			= $request->comments;
		$fullfillment       = $request->fullfillment;
		
		
		
		
		$recaptcha_response	= $request->recaptcha_response;
		
		if($this->recaptchaVerification($recaptcha_response))
        {
          
    		DB::table('quotation_submission')
    				->insert([					
    					'qt_card_type'	=> $card_type,
    					'qt_no_of_cards'	=> $no_of_cards,
    					'qt_features'	=> $features,
    					'qt_cardthickness'	=> $cardthickness,
    					'qt_no_of_colors_front'	=> $no_of_colors_front,
    					'qt_no_of_colors_back'	 => $no_of_colors_back,
    					'qt_magnetic_stripe'	 => $magnetic_stripe,
    					'qt_date_card'	 => $date_card,
    					'qt_fullfillment'	 => $fullfillment,
    					'qt_how_findus'	 => $how_findus,
    					'qt_firstname'	 => $firstname,
    					'qt_lastname'	 => $lastname,
    					'qt_email'	 => $c_email,
    					'qt_phone'	 => $c_phone,
    					'qt_comments'	 => $comments
    					
    					]);
    
    		$to_email		= "info@plastic-cards.co.nz";

    		$content = array();	
    		$content['card_type']= $card_type;
    		$content['no_of_cards']= $no_of_cards;
    		$content['features']  = $features;
    		$content['cardthickness']     = $cardthickness;
    		$content['no_of_colors_front']	 = $no_of_colors_front;
    		$content['no_of_colors_back']	 = $no_of_colors_back;
    		$content['magnetic_stripe']	 = $magnetic_stripe;
    		$content['date_card']	 = $date_card;
    		$content['how_findus']	 = $how_findus;
    		$content['fullfillment']	 = $fullfillment;
    		$content['firstname']	 = $firstname;
    		$content['lastname']	 = $lastname;
    		$content['c_email']	 = $c_email;
    		$content['c_phone']	 = $c_phone;
    		$content['comments']	 = $comments;
    					
    		//mail session starts here
    		//	$content = [];
    			Mail::to($to_email)->queue(new QuoteSubmit($content));
    			
    			Mail::to($c_email)->queue(new QuoteAutoResponder($content));//auto responder to customer
    				
    		//mail session ends here		
    		return Redirect::to(route('thank-you.html'))->send();
        }
        else
    	{
    	    
    	    return Redirect::back()->withErrors(['msg', 'The Message']);
    	}
	}
	
	public function order_sample_submit(Request $request) {
	
		

		$firstname	= $request->firstname;
		$lastname	= $request->lastname;
		$company	= $request->company;
		$address1	= $request->address1;
		$address2	= $request->address2;
		$city		= $request->city;
		$state		= $request->state;
		$zip		= $request->zip;
		$country	= $request->country;
		$email		= $request->email;		
		$phone		= $request->phone;
		$how_findus	= $request->how_findus;
		$comments	= $request->comments;
		
		$recaptcha_response	= $request->recaptcha_response;
		
		if($this->recaptchaVerification($recaptcha_response))
        {
    		DB::table('ordersamples')
    				->insert([					
    					'ord_firstname'	=> $firstname,
    					'ord_lastname'	=> $lastname,
    					'ord_company'	=> $company,
    					'ord_address1'	=> $address1,
    					'ord_address2'	=> $address2,
    					'ord_city'	 => $city,
    					'ord_state'	 => $state,
    					'ord_zip'	 => $zip,
    					'ord_country'	 => $country,
    					'ord_email'	 => $email,
    					'ord_phone'	 => $phone,
    					'ord_how_findus'	 => $how_findus,				
    					'ord_comments'	 => $comments
    					
    					]);
    
    		$to_email		= "info@plastic-cards.co.nz";
    		
    		$content = array();	
    		$content['firstname']	=  $firstname;
    		$content['lastname']	=  $lastname;
    		$content['company']		=  $company;
    		$content['address1'] 	=  $address1;
    		$content['address2'] 	=  $address2;
    		$content['city'] 		=  $city;
    		$content['state'] 		=  $state;
    		$content['zip'] 		=  $zip;
    		$content['country'] 	=  $country;
    		$content['email'] 		=  $email;		
    		$content['phone'] 		=  $phone;
    		$content['how_findus'] 	=  $how_findus;
    		$content['comments'] 	=  $comments;
    					
    		//mail session starts here
    		//	$content = [];
    			Mail::to($to_email)->queue(new SampleOrder($content));
    		//mail session ends here		
    		return Redirect::to(route('thank-you.html'))->send();
        }
        else
    	{
    	    
    	    return Redirect::back()->withErrors(['msg', 'The Message']);
    	}
	}
	
	
	
    
    


}

?>