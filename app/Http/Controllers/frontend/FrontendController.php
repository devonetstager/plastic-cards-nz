<?php
namespace App\Http\Controllers\frontend;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
//use Illuminate\Support\Facades\Paginator;
use View;use Mail;use Redirect;
use DB;use Auth;use Input;use Hash;use Validator;use Crypt;
use Request; 
use Route;
use Illuminate\Http\Response; 
use Session; use Carbon;



class FrontendController extends Controller
{
	public function __construct(){
		ini_set("memory_limit","500M"); 
	}
	
	
	
	public function home($datas=array()){
		
		$activeMenu	= 'home';
		$data = DB::table('homepage')              
				->where('id',1)
				->first();
				
		$hbox = DB::table('homepage_boxes')
				->orderBy('hbox_order','asc')				
				->get();		
		$hclients = DB::table('homepage_clients')
				->orderBy('clients_order','asc') 
				
				->get();
				
		$testimonials = DB::table('testimonials')
				->orderBy('tes_order','desc')    
				->limit(4)
				
				->get();
				
		$options = DB::table('options')
							->where('id',1)
                            ->first();
		
		return View::make('frontend.home')->with([
				'activeMenu'=>$activeMenu,
				'data'=>$data,
				'hbox'=>$hbox,
				'hclients'=>$hclients,
				'testimonials'=>$testimonials,
				'options'=>$options
		])->render();
	}
	
	public function pages(){
		
		$slug			= urldecode(Request::segment(1));
		$datas['slug']	= $slug;
		
		$options = DB::table('options')
							->where('id',1)
                            ->first();
		
		$slugData = DB::table('slug as s')
								->join('pages as p', 'p.id', '=', 's.page_id')
								->select('s.page_id','s.template')
								->where('s.slug_url',$slug)
								->get();
			
		if(count($slugData)>0)
		{
			$datas['page_id'] 	= $slugData[0]->page_id;
			$template			= $slugData[0]->template;
				
		
		
			$activeMenu	= $datas['slug'];
			$data = DB::table('pages')              
					->where('id',$datas['page_id'])
					->first();
			if($template=='gallery')
			{
				$galleryPages = DB::table('pages as p')   
					->join('slug as s', 'p.id', '=', 's.page_id')
					->where('page_parent',$datas['page_id'])
					->get();
				
				return View::make('frontend.gallery.gallery_list')->with([
						'activeMenu'=>$activeMenu,
						'data'=>$data,
						'galleryPages'=>$galleryPages,
						'options'=>$options
				])->render();
				
				
			}
			else if($template=='gallery_detail')
			{
				$galleryImages = DB::table('gallery')   
					
					->where('gallery_page_id',$datas['page_id'])
					->get();
				
				return View::make('frontend.gallery.gallery_details')->with([
						'activeMenu'=>$activeMenu,
						'data'=>$data,
						'galleryImages'=>$galleryImages,
						'options'=>$options
				])->render();
				
				
			}
			else if($template=='contactus')
			{
				return View::make('frontend.contact.contact_us')->with([
						'activeMenu'=>$activeMenu,
						'data'=>$data,						
						'options'=>$options
				])->render();
				
				
			}
			else if($template=='ordersamples')
			{
				return View::make('frontend.ordersamples.ordersamples')->with([
						'activeMenu'=>$activeMenu,
						'data'=>$data,						
						'options'=>$options
				])->render();
				
				
			}
			else if($template=='testimonial')
			{
				$testimonials = DB::table('testimonials')
				->orderBy('tes_order','desc')    
              
				
				->get();
				return View::make('frontend.testimonial.testimonial')->with([
						'activeMenu'=>$activeMenu,
						'data'=>$data,						
						'testimonials'=>$testimonials,						
						'options'=>$options
				])->render();
				
				
			}
			else if($template=='quote')
			{
				return View::make('frontend.quote.quote')->with([
						'activeMenu'=>$activeMenu,
						'data'=>$data,						
						'options'=>$options
				])->render();
				
				
			}
			else if($template=='submitorder')
			{
				return View::make('frontend.submitorder.submitorder')->with([
						'activeMenu'=>$activeMenu,
						'data'=>$data,						
						'options'=>$options
				])->render();
				
				
			}
			else
			{
				return View::make('frontend.common')->with([
						'activeMenu'=>$activeMenu,
						'data'=>$data,
						'options'=>$options
				])->render();
			}
		}
		else
		{
			$activeMenu = "404";
			$options = DB::table('options')
							->where('id',1)
                            ->first();
			$data = DB::table('pages')              
						->where('id',23)
						->first();
			//print_r($data);	exit();		
			return View::make('frontend.common')->with([
							'activeMenu'=>$activeMenu,
							'data'=>$data,
							'options'=>$options
					])->render();
		}
	}
	
	
	
	public function error($data=array()) {
		echo "dd";
		$activeMenu = "404";
		
		
  } 
    
    


}

?>