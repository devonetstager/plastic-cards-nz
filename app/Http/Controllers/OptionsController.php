<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use App\options;

class OptionsController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
	
	public function index()
    {
		/*$options = new options;
		$options->youtube = 'Youtube';
		$options->linkedin = 'https://in.linkedin.com/';
		
		$options->save();*/
		$id	= 1;//preset
		$options = options::where('id' ,$id)->first(); 
       
		
        return view('cp-admin.options.options', compact('options'));
    }
	public function update(Request $request){
		$this->validate($request, [
            "email" => "required|email",
			"phonenumber" => "regex:/^[\s\d\+]+$/",
			"mobile" => "regex:/^[\s\d\+]+$/",
        ],[
            'email.required' => 'This must be a valid email address',		
			'phonenumber.regex' => 'Incorrect Format',
			'mobile.regex' => 'Incorrect Format'
        ]);
		
		
		$id = 1;
		//list of form fields
		$fill_items = array("youtube", "linkedin", "googleplus", "instagram", "twitter", "facebook", "address", "email", "phonenumber", "mobile");
		//create data array
		foreach($fill_items as $keys)
		{
			$data_array[$keys] = $request -> $keys;
        
		}
		
        $data_update = options::where('id' , $id)->update($data_array);
        if($data_update)
            $request->session()->flash('success', 'Options updated successfully');
        else
            $request->session()->flash('info', "Can't updated options , try later.");
        return Redirect::back();
    }
	
}
