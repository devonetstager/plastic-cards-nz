<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/clear-cache', function() {
	Artisan::call('config:clear');
	Artisan::call('cache:clear');
	Artisan::call('config:cache');
	Artisan::call('view:cache');
	Artisan::call('view:clear');
	return 'DONE'; 
});

Route::get('/', function () {
    return view('welcome');
});






	//Auth::routes();
	
	Auth::routes([ 'register' => false ]);

	Route::get('cp-admin/home', 'HomeController@index')->name('home');
	Route::get('cp-admin/option', 'OptionsController@index')->name('option');
	Route::post('cp-admin/option/update', 'OptionsController@update')->name('option_update');

	//Admin pages roots starts
	Route::get('cp-admin/create_page', 'PageController@create')->name('create_page');
	Route::get('cp-admin/list_page', 'PageController@index')->name('list_page');
	Route::get('cp-admin/edit_page/{id}', 'PageController@edit_page')->name('edit_page');
	Route::post('cp-admin/pages/update', 'PageController@update_page')->name('update_page');
	Route::post('upload_content_image', 'PageController@upload_content_image')->name('upload_content_image');

	Route::post('upload_image','PageController@upload_image')->name('upload_image');
	Route::get('delete_image','PageController@delete_image')->name('delete_image');


	//Admin pages roots ends

	//Admin Home roots starts
	Route::get('cp-admin/mainpage', 'HomeController@mainpage')->name('mainpage');
	Route::post('cp-admin/update_mainpage', 'HomeController@update_mainpage')->name('update_mainpage');

	//home boxes
	Route::get('cp-admin/home_box', 'HomeController@home_box')->name('home_box');
	Route::get('cp-admin/home_box_edit/{id}', 'HomeController@home_box_edit')->name('home_box_edit');
	Route::post('cp-admin/home_box_update', 'HomeController@home_box_update')->name('home_box_update');

	//home our clients
	Route::get('cp-admin/home_clients_add', 'HomeController@home_clients_add')->name('home_clients_add');
	Route::get('cp-admin/home_clients', 'HomeController@home_clients')->name('home_clients');
	Route::get('cp-admin/home_clients_edit/{id}', 'HomeController@home_clients_edit')->name('home_clients_edit');
	Route::post('cp-admin/home_clients_update', 'HomeController@home_clients_update')->name('home_clients_update');
	Route::post('cp-admin/home_clients_save', 'HomeController@home_clients_save')->name('home_clients_save');
	Route::get('cp-admin/home_clients_delete/{id}', 'HomeController@home_clients_delete')->name('home_clients_delete');
	
	
	//Testimonial
	Route::get('cp-admin/testimonial_add', 'TestimonialController@testimonial_add')->name('testimonial_add');
	Route::get('cp-admin/testimonial', 'TestimonialController@testimonial')->name('testimonial');
	Route::get('cp-admin/testimonial_edit/{id}', 'TestimonialController@testimonial_edit')->name('testimonial_edit');
	Route::post('cp-admin/testimonial_update', 'TestimonialController@testimonial_update')->name('testimonial_update');
	Route::post('cp-admin/testimonial_save', 'TestimonialController@testimonial_save')->name('testimonial_save');
	Route::get('cp-admin/testimonial_delete/{id}', 'TestimonialController@testimonial_delete')->name('testimonial_delete');
	
	//Froala Editor image upload
	
	Route::post('froala_upload', 'FroalaController@froala_upload')->name('froala_upload');
	
	//sorting 
	Route::get('sort_order',['as'=>'sort_order','uses'=>'HomeController@sort_order']);
	
	
	//Submission listing
	
	Route::get('cp-admin/contact_submissions', 'SubmissionController@contact_submissions')->name('contact_submissions');
	Route::get('cp-admin/contact_submissions_delete/{id}', 'SubmissionController@contact_submissions_delete')->name('contact_submissions_delete');
	
	Route::get('cp-admin/quote_submissions', 'SubmissionController@quote_submissions')->name('quote_submissions');
	Route::get('cp-admin/quote_submissions_delete/{id}', 'SubmissionController@quote_submissions_delete')->name('quote_submissions_delete');
	
	
	Route::get('cp-admin/order_submissions', 'SubmissionController@order_submissions')->name('order_submissions');
	Route::get('cp-admin/order_submissions_delete/{id}', 'SubmissionController@order_submissions_delete')->name('order_submissions_delete');
	
	Route::get('cp-admin/order_sample_submissions', 'SubmissionController@order_sample_submissions')->name('order_sample_submissions');
	Route::get('cp-admin/order_sample_submissions_delete/{id}', 'SubmissionController@order_sample_submissions_delete')->name('order_sample_submissions_delete');
	
	//Admin Home roots ends
	
    Route::get('cp-admin/change_password', 'UserController@change_password')->name('change_password');
	


	Route::post('updating_password', 'UserController@updating_password')->name('updating_password');
	
	
	
	




/*FROND END ROUTES*/
	
  Route::post('contact_submit', 'frontend\FormsController@contact_submission')->name('contact_submission');
  Route::post('order_submit', 'frontend\FormsController@order_submit')->name('order_submit');
  Route::post('quote_submit', 'frontend\FormsController@quote_submit')->name('quote_submit');
  Route::post('order_sample_submit', 'frontend\FormsController@order_sample_submit')->name('order_sample_submit');
  
    Route::group(['prefix'=>''],function(){

	Route::any('thank-you.html',['as'=>'thank-you.html','uses'=>'frontend\FrontendController@pages']);
  
    	
		/*Index routes */
			Route::any('/{url?}',['as'=>'pages','uses'=>'frontend\FrontendController@pages']);
			Route::any('/',['as'=>'home','uses'=>'frontend\FrontendController@home']);
			
		/*Index routes end */


	});

/*FROND END ROUTES*/