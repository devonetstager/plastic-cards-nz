-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 12, 2020 at 11:17 PM
-- Server version: 5.6.46-cll-lve
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `plasticardnz`
--

-- --------------------------------------------------------

--
-- Table structure for table `contact_submissions`
--

CREATE TABLE `contact_submissions` (
  `con_id` int(11) NOT NULL,
  `con_firstname` varchar(100) DEFAULT NULL,
  `con_lastname` varchar(100) DEFAULT NULL,
  `con_email` varchar(500) DEFAULT NULL,
  `con_inquiry_type` varchar(100) DEFAULT NULL,
  `con_inquiry` text,
  `con_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_submissions`
--

INSERT INTO `contact_submissions` (`con_id`, `con_firstname`, `con_lastname`, `con_email`, `con_inquiry_type`, `con_inquiry`, `con_date`) VALUES
(1, 'Test', 'te', 'test@gmil.com', 'Sales', 'test test', '2019-05-28 12:15:29'),
(2, 'Test', 'test', 'qacheck17@gmail.com', 'General Comments', 'test test test', '2019-05-28 12:43:51'),
(3, 'ffff', 'gg', 'qacheck17@gmail.com', 'Inquiry', 'gfgdfg', '2019-05-28 12:45:13'),
(4, 'Ttest', 'test', 'qacheck17@gmail.com', 'Inquiry', 'dsgdsg', '2019-05-28 12:46:06'),
(5, 'test', 't', 'test@gmail.com', 'Inquiry', 'test test', '2019-05-29 13:30:17'),
(6, 'Tester', 'test', 'test@gmail.com', 'Sales', 'test', '2019-05-29 19:01:36'),
(7, 'Test', 'test', 'test@gmail.com', 'General Comments', 'dfdsf sdfdsfds', '2019-05-29 20:12:35'),
(8, 'Test', 'T', 'test@gmail.com', 'General Comments', 'test test', '2019-05-30 17:38:23'),
(9, 'Test', 'T', 'test@gmail.com', 'General Comments', 'test test', '2019-05-30 17:40:02'),
(10, 'test', 'tes', 'test@gmail.com', 'Sales', 'gdfgd', '2019-05-30 17:42:30'),
(11, 'Test', 'tes', 'test@gmail.com', 'Inquiry', 'test test', '2019-05-30 17:45:27'),
(12, 'Alex Test', 'Test', 'amanoago@clixpert.com.au', 'General Comments', 'Test', '2019-05-31 07:33:05'),
(13, 'test test', 'a', 'test@gmail.com', 'Inquiry', 'test test', '2019-07-01 02:05:55'),
(14, 'Clixpert Test', 't', 'qacheck17@gmail.com', NULL, 'test', '2019-07-01 02:43:28'),
(15, 'Clixpert', 'Test', 'qacheck17@gmail.com', 'Inquiry', 'test testt ette', '2019-07-01 05:08:04'),
(16, 'Alex Test', 'Test', 'amaniago@clixpert.com.au', 'Sales', 'Test', '2019-07-01 05:22:32'),
(17, 'Alex', 'Test 2', 'amaniago@clixpert.com.au', 'Inquiry', 'test', '2019-07-01 16:34:04'),
(18, 'Alex', 'Test', 'amaniago@clixpert.com.au', 'General Comments', 'test', '2019-07-01 18:44:13'),
(19, 'Alex Test', 'Test 1', 'amaniago@clixpert.com.au', 'Inquiry', 'test', '2019-07-01 21:27:10'),
(20, 'Clixpert', 'Test', 'qacheck2018@gmail.com', 'General Comments', 'Test comment', '2019-07-03 22:33:22'),
(21, 'Alex', 'Test', 'amaniago@clixpert.com.au', 'Sales', 'test', '2019-07-04 23:16:47'),
(22, 'Anaru', 'Ratapu', 'mahikainganz@gmail.com', NULL, 'Hi there can you give a quote for 100 membership cards', '2019-07-11 18:40:35'),
(23, 'ADRIAN', 'an', 'whiplashshunyang@gmail.com', 'Sales', 'what is the price for 200 credit card size card?\r\nwe have our own design, just need cards and printing services.', '2019-07-25 23:00:44'),
(24, 'test', 'testst', 'tstesrr@gmail.com', 'Sales', 'test', '2019-09-02 04:02:34'),
(25, 'test', 'test', 'test@gmail.com', NULL, 'test message', '2019-09-03 20:36:47'),
(26, 'test', 'testst', 'tstesrr@gmail.com', 'Other', 'test', '2019-09-04 11:07:34'),
(27, 'Alex', 'Test', 'amaniago@clixpert.com.au', 'Inquiry', 'test', '2019-09-23 17:04:37'),
(28, 'Tom', 'Gray', 'buller.printing@xtra.co.nz', 'Sales', 'Hi team…\r\nI have a client who uses Personalised lock-out tags around various sites in New Zealand.\r\nCurrently they are using a cumbersome I.D. card that is too large and they want to use a credit card size for future use.\r\nI have attached a typical sample of the round-cornered type they want to use.\r\nThey are 2-sided.\r\nThey have about 450 individuals and they each have two tags in case one gets lost.\r\nThey are ordered randomly as and when new staff are taken on, so not ordered as a bulk run.\r\nIs this something that you can provide as an ongoing service and if so….can you advise a unit cost.\r\nWe would provide the artwork to your specs, or would you prefer to do this?\r\nAs an extra service, could there be a 5mm hole at the top and if so, can you advise the separate cost for this as well.\r\nIf not…..maybe just a slot?\r\nRegards\r\nTom', '2019-11-06 20:31:40');

-- --------------------------------------------------------

--
-- Table structure for table `cpau_cmsv_delete`
--

CREATE TABLE `cpau_cmsv_delete` (
  `cms_id` bigint(20) UNSIGNED NOT NULL,
  `cms_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cms_description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cms_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `gallery_id` int(11) NOT NULL,
  `gallery_page_id` int(11) DEFAULT NULL,
  `gallery_original_name` varchar(500) DEFAULT NULL,
  `gallery_file` varchar(500) DEFAULT NULL,
  `gallery_image_order` int(11) NOT NULL DEFAULT '0',
  `gallery_trash_status` tinyint(1) DEFAULT '0',
  `gallery_created_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`gallery_id`, `gallery_page_id`, `gallery_original_name`, `gallery_file`, `gallery_image_order`, `gallery_trash_status`, `gallery_created_date`) VALUES
(17, 9, '3.jpg', 'gallery_155878855986.jpg', 0, 0, '2019-05-25 12:49:19'),
(18, 9, '4.jpg', 'gallery_155878855914.jpg', 0, 0, '2019-05-25 12:49:19'),
(19, 9, '2.jpg', 'gallery_155878855931.jpg', 0, 0, '2019-05-25 12:49:19'),
(20, 9, '1.jpg', 'gallery_155878855928.jpg', 0, 0, '2019-05-25 12:49:19'),
(21, 9, '18.jpg', 'gallery_155878855941.jpg', 0, 0, '2019-05-25 12:49:19'),
(22, 9, '5.jpg', 'gallery_155878855974.jpg', 0, 0, '2019-05-25 12:49:19'),
(23, 9, '6.jpg', 'gallery_155878856138.jpg', 0, 0, '2019-05-25 12:49:21'),
(24, 9, '10.jpg', 'gallery_155878856124.jpg', 0, 0, '2019-05-25 12:49:21'),
(25, 9, '9.jpg', 'gallery_155878856171.jpg', 0, 0, '2019-05-25 12:49:21'),
(26, 9, '8.jpg', 'gallery_155878856145.jpg', 0, 0, '2019-05-25 12:49:21'),
(27, 9, '11.jpg', 'gallery_155878856163.jpg', 0, 0, '2019-05-25 12:49:21'),
(28, 9, '7.jpg', 'gallery_155878856115.jpg', 0, 0, '2019-05-25 12:49:21'),
(29, 9, '14.jpg', 'gallery_155878856275.jpg', 0, 0, '2019-05-25 12:49:22'),
(30, 9, '16.jpg', 'gallery_155878856215.jpg', 0, 0, '2019-05-25 12:49:22'),
(31, 9, '12.jpg', 'gallery_155878856251.jpg', 0, 0, '2019-05-25 12:49:22'),
(32, 9, '13.jpg', 'gallery_155878856277.jpg', 0, 0, '2019-05-25 12:49:22'),
(33, 9, '15.jpg', 'gallery_155878856277.jpg', 0, 0, '2019-05-25 12:49:22'),
(34, 9, '17.jpg', 'gallery_155878856282.jpg', 0, 0, '2019-05-25 12:49:22'),
(57, 11, 'access2.jpg', 'gallery_155879969593.jpg', 0, 0, '2019-05-25 15:54:55'),
(58, 11, 'memcard14.jpg', 'gallery_155879969586.jpg', 0, 0, '2019-05-25 15:54:55'),
(59, 11, 'memcard5.jpg', 'gallery_155879969556.jpg', 0, 0, '2019-05-25 15:54:55'),
(60, 11, 'memcard11.jpg', 'gallery_155879969581.jpg', 0, 0, '2019-05-25 15:54:55'),
(61, 11, 'adv17.jpg', 'gallery_155879969544.jpg', 0, 0, '2019-05-25 15:54:55'),
(62, 11, 'adv14-front.jpg', 'gallery_155879969671.jpg', 0, 0, '2019-05-25 15:54:56'),
(63, 11, 'loyal11.jpg', 'gallery_155879969618.jpg', 0, 0, '2019-05-25 15:54:56'),
(64, 11, 'gc5.jpg', 'gallery_155879969654.jpg', 0, 0, '2019-05-25 15:54:56'),
(65, 11, 'disc9.jpg', 'gallery_155879969635.jpg', 0, 0, '2019-05-25 15:54:56'),
(66, 12, 'access2.jpg', 'gallery_155879982079.jpg', 0, 0, '2019-05-25 15:57:00'),
(67, 12, 'loyal5.jpg', 'gallery_155879982068.jpg', 0, 0, '2019-05-25 15:57:00'),
(68, 12, 'loyal7.jpg', 'gallery_155879982062.jpg', 0, 0, '2019-05-25 15:57:00'),
(69, 12, 'loyal6.jpg', 'gallery_155879982080.jpg', 0, 0, '2019-05-25 15:57:00'),
(70, 12, 'memcard5.jpg', 'gallery_155879982027.jpg', 0, 0, '2019-05-25 15:57:00'),
(71, 12, 'adv14-front.jpg', 'gallery_155879982093.jpg', 0, 0, '2019-05-25 15:57:00'),
(72, 12, 'adv17.jpg', 'gallery_155879982099.jpg', 0, 0, '2019-05-25 15:57:00'),
(73, 12, 'loyal1.jpg', 'gallery_155879982170.jpg', 0, 0, '2019-05-25 15:57:01'),
(74, 12, 'disc7.jpg', 'gallery_155879982134.jpg', 0, 0, '2019-05-25 15:57:01'),
(75, 13, 'memcard11.jpg', 'gallery_155879997913.jpg', 0, 0, '2019-05-25 15:59:39'),
(76, 13, 'adv14-front.jpg', 'gallery_155879997919.jpg', 0, 0, '2019-05-25 15:59:39'),
(77, 13, 'disc9.jpg', 'gallery_155879997955.jpg', 0, 0, '2019-05-25 15:59:39'),
(78, 13, 'disc7.jpg', 'gallery_155879997935.jpg', 0, 0, '2019-05-25 15:59:39'),
(79, 13, 'memcard5.jpg', 'gallery_155879997971.jpg', 0, 0, '2019-05-25 15:59:39'),
(80, 13, 'memcard14.jpg', 'gallery_155879997993.jpg', 0, 0, '2019-05-25 15:59:39'),
(81, 13, 'gc4-front.jpg', 'gallery_155879998064.jpg', 0, 0, '2019-05-25 15:59:40'),
(82, 13, 'gc5.jpg', 'gallery_155879998078.jpg', 0, 0, '2019-05-25 15:59:40'),
(83, 13, 'loyal7.jpg', 'gallery_155879998032.jpg', 0, 0, '2019-05-25 15:59:40'),
(84, 14, 'custom8.jpg', 'gallery_155880005898.jpg', 0, 0, '2019-05-25 16:00:58'),
(85, 14, 'custom7.jpg', 'gallery_155880005832.jpg', 0, 0, '2019-05-25 16:00:58'),
(86, 14, 'custom1.jpg', 'gallery_155880005852.jpg', 0, 0, '2019-05-25 16:00:58'),
(87, 14, 'custom2.jpg', 'gallery_155880005929.jpg', 0, 0, '2019-05-25 16:00:59'),
(88, 14, 'custom5.jpg', 'gallery_155880005930.jpg', 0, 0, '2019-05-25 16:00:59'),
(90, 14, 'custom6.jpg', 'gallery_155880005990.jpg', 0, 0, '2019-05-25 16:00:59'),
(106, 15, 'mailer1a.jpg', 'gallery_155911869059.jpg', 0, 0, '2019-05-29 08:31:30'),
(107, 15, 'mailer2a.jpg', 'gallery_155911891757.jpg', 0, 0, '2019-05-29 08:35:17'),
(108, 15, 'mail5.jpg', 'gallery_155911891786.jpg', 0, 0, '2019-05-29 08:35:17'),
(109, 15, 'mail4.jpg', 'gallery_155911891787.jpg', 0, 0, '2019-05-29 08:35:17'),
(151, 24, 'plastic-cards-australia-business-card _4_.webp', 'plastic-cards-australia-business-card-4.webp', 0, 0, '2019-05-31 04:58:24'),
(152, 24, 'plastic-cards-australia-business-card _9_.webp', 'plastic-cards-australia-business-card-9.webp', 0, 0, '2019-05-31 04:58:24'),
(153, 24, 'plastic-cards-australia-business-card _6_.webp', 'plastic-cards-australia-business-card-6.webp', 0, 0, '2019-05-31 04:58:24'),
(154, 24, 'plastic-cards-australia-business-card _7_.webp', 'plastic-cards-australia-business-card-7.webp', 0, 0, '2019-05-31 04:58:24'),
(155, 24, 'plastic-cards-australia-business-card _3_.webp', 'plastic-cards-australia-business-card-3.webp', 0, 0, '2019-05-31 04:58:24'),
(156, 24, 'plastic-cards-australia-business-card _2_.webp', 'plastic-cards-australia-business-card-2.webp', 0, 0, '2019-05-31 04:58:24'),
(157, 24, 'plastic-cards-australia-business-card _8_.webp', 'plastic-cards-australia-business-card-8.webp', 0, 0, '2019-05-31 04:58:24'),
(158, 24, 'plastic-cards-australia-business-card _5_.webp', 'plastic-cards-australia-business-card-5.webp', 0, 0, '2019-05-31 04:58:24'),
(159, 24, 'plastic-cards-australia-business-card _1_.webp', 'plastic-cards-australia-business-card-1.webp', 0, 0, '2019-05-31 04:58:24'),
(176, 15, 'plastic-cards-australia-mailers.jpg', 'plastic-cards-australia-mailers.jpg', 0, 0, '2019-07-01 10:10:19'),
(177, 14, 'plastic-cards-australia-custom-cards.jpg', 'plastic-cards-australia-custom-cards.jpg', 0, 0, '2019-07-01 10:11:28'),
(178, 15, 'a1.jpg', 'a1.jpg', 0, 0, '2019-07-01 11:22:05'),
(179, 15, 'a10.jpg', 'a10.jpg', 0, 0, '2019-07-01 11:22:06'),
(180, 15, 'a11.jpg', 'a11.jpg', 0, 0, '2019-07-01 11:22:06'),
(181, 15, 'a12.jpg', 'a12.jpg', 0, 0, '2019-07-01 11:22:07'),
(182, 15, 'a13.jpg', 'a13.jpg', 0, 0, '2019-07-01 11:22:07'),
(183, 15, 'a14.jpg', 'a14.jpg', 0, 0, '2019-07-01 11:22:07'),
(184, 15, 'a15.jpg', 'a15.jpg', 0, 0, '2019-07-01 11:22:08'),
(185, 15, 'a16.jpg', 'a16.jpg', 0, 0, '2019-07-01 11:22:08'),
(186, 15, 'a9.jpg', 'a9.jpg', 0, 0, '2019-07-01 11:22:09'),
(187, 15, 'a6.jpg', 'a6.jpg', 0, 0, '2019-07-01 11:22:09'),
(188, 15, 'a8.jpg', 'a8.jpg', 0, 0, '2019-07-01 11:22:09'),
(189, 15, 'a2.jpg', 'a2.jpg', 0, 0, '2019-07-01 11:22:10'),
(190, 15, 'a5.jpg', 'a5.jpg', 0, 0, '2019-07-01 11:22:10'),
(191, 15, 'a3.jpg', 'a3.jpg', 0, 0, '2019-07-01 11:22:10'),
(192, 15, 'a7.jpg', 'a7.jpg', 0, 0, '2019-07-01 11:22:10'),
(193, 15, 'a4.jpg', 'a4.jpg', 0, 0, '2019-07-01 11:22:10'),
(194, 8, 'plastic-cards-new-zealand-credit-card-size-cards-1.jpg', 'plastic-cards-new-zealand-credit-card-size-cards-1.jpg', 0, 0, '2019-07-05 23:23:51'),
(195, 8, 'plastic-cards-new-zealand-credit-card-size-cards-5.jpg', 'plastic-cards-new-zealand-credit-card-size-cards-5.jpg', 0, 0, '2019-07-05 23:23:51'),
(196, 8, 'plastic-cards-new-zealand-credit-card-size-cards-3.jpg', 'plastic-cards-new-zealand-credit-card-size-cards-3.jpg', 0, 0, '2019-07-05 23:23:54'),
(197, 8, 'plastic-cards-new-zealand-credit-card-size-cards-8.jpg', 'plastic-cards-new-zealand-credit-card-size-cards-8.jpg', 0, 0, '2019-07-05 23:23:54'),
(198, 8, 'plastic-cards-new-zealand-credit-card-size-cards-4.jpg', 'plastic-cards-new-zealand-credit-card-size-cards-4.jpg', 0, 0, '2019-07-05 23:23:54'),
(199, 8, 'plastic-cards-new-zealand-credit-card-size-cards-2.jpg', 'plastic-cards-new-zealand-credit-card-size-cards-2.jpg', 0, 0, '2019-07-05 23:23:54'),
(200, 8, 'plastic-cards-new-zealand-credit-card-size-cards-7.jpg', 'plastic-cards-new-zealand-credit-card-size-cards-7.jpg', 0, 0, '2019-07-05 23:23:54'),
(201, 8, 'plastic-cards-new-zealand-credit-card-size-cards-6.jpg', 'plastic-cards-new-zealand-credit-card-size-cards-6.jpg', 0, 0, '2019-07-05 23:23:54'),
(202, 8, 'plastic-cards-new-zealand-credit-card-size-cards-14.jpg', 'plastic-cards-new-zealand-credit-card-size-cards-14.jpg', 0, 0, '2019-07-05 23:23:56'),
(203, 8, 'plastic-cards-new-zealand-credit-card-size-cards-9.jpg', 'plastic-cards-new-zealand-credit-card-size-cards-9.jpg', 0, 0, '2019-07-05 23:23:56'),
(204, 8, 'plastic-cards-new-zealand-credit-card-size-cards-11.jpg', 'plastic-cards-new-zealand-credit-card-size-cards-11.jpg', 0, 0, '2019-07-05 23:23:56'),
(205, 8, 'plastic-cards-new-zealand-credit-card-size-cards-17.jpg', 'plastic-cards-new-zealand-credit-card-size-cards-17.jpg', 0, 0, '2019-07-05 23:23:56'),
(206, 8, 'plastic-cards-new-zealand-credit-card-size-cards-15.jpg', 'plastic-cards-new-zealand-credit-card-size-cards-15.jpg', 0, 0, '2019-07-05 23:23:56'),
(207, 8, 'plastic-cards-new-zealand-credit-card-size-cards-13.jpg', 'plastic-cards-new-zealand-credit-card-size-cards-13.jpg', 0, 0, '2019-07-05 23:23:56'),
(208, 8, 'plastic-cards-new-zealand-credit-card-size-cards-16.jpg', 'plastic-cards-new-zealand-credit-card-size-cards-16.jpg', 0, 0, '2019-07-05 23:23:56'),
(209, 8, 'plastic-cards-new-zealand-credit-card-size-cards-12.jpg', 'plastic-cards-new-zealand-credit-card-size-cards-12.jpg', 0, 0, '2019-07-05 23:23:56'),
(210, 8, 'plastic-cards-new-zealand-credit-card-size-cards-10.jpg', 'plastic-cards-new-zealand-credit-card-size-cards-10.jpg', 0, 0, '2019-07-05 23:23:56'),
(212, 10, 'plastic-cards-new-zealand-combo-tag-9.jpg', 'plastic-cards-new-zealand-combo-tag-9.jpg', 0, 0, '2019-07-05 23:30:14'),
(213, 10, 'plastic-cards-new-zealand-combo-tag-14.jpg', 'plastic-cards-new-zealand-combo-tag-14.jpg', 0, 0, '2019-07-05 23:30:17'),
(214, 10, 'plastic-cards-new-zealand-combo-tag-11.jpg', 'plastic-cards-new-zealand-combo-tag-11.jpg', 0, 0, '2019-07-05 23:30:17'),
(215, 10, 'plastic-cards-new-zealand-combo-tag-8.jpg', 'plastic-cards-new-zealand-combo-tag-8.jpg', 0, 0, '2019-07-05 23:30:17'),
(216, 10, 'plastic-cards-new-zealand-combo-tag-7.jpg', 'plastic-cards-new-zealand-combo-tag-7.jpg', 0, 0, '2019-07-05 23:30:17'),
(217, 10, 'plastic-cards-new-zealand-combo-tag-10.jpg', 'plastic-cards-new-zealand-combo-tag-10.jpg', 0, 0, '2019-07-05 23:30:17'),
(218, 10, 'plastic-cards-new-zealand-combo-tag-13.jpg', 'plastic-cards-new-zealand-combo-tag-13.jpg', 0, 0, '2019-07-05 23:30:17'),
(219, 10, 'plastic-cards-new-zealand-combo-tag-12.jpg', 'plastic-cards-new-zealand-combo-tag-12.jpg', 0, 0, '2019-07-05 23:30:17'),
(220, 10, 'plastic-cards-new-zealand-combo-tag-4.jpg', 'plastic-cards-new-zealand-combo-tag-4.jpg', 0, 0, '2019-07-05 23:30:19'),
(221, 10, 'plastic-cards-new-zealand-combo-tag-1.jpg', 'plastic-cards-new-zealand-combo-tag-1.jpg', 0, 0, '2019-07-05 23:30:19'),
(222, 10, 'plastic-cards-new-zealand-combo-tag-3.jpg', 'plastic-cards-new-zealand-combo-tag-3.jpg', 0, 0, '2019-07-05 23:30:19'),
(223, 10, 'plastic-cards-new-zealand-combo-tag-2.jpg', 'plastic-cards-new-zealand-combo-tag-2.jpg', 0, 0, '2019-07-05 23:30:19'),
(224, 10, 'plastic-cards-new-zealand-combo-tag-6.jpg', 'plastic-cards-new-zealand-combo-tag-6.jpg', 0, 0, '2019-07-05 23:30:19'),
(225, 10, 'plastic-cards-new-zealand-combo-tag-5.jpg', 'plastic-cards-new-zealand-combo-tag-5.jpg', 0, 0, '2019-07-05 23:30:19'),
(226, 10, 'plastic-cards-new-zealand-combo-tag-15.jpg', 'plastic-cards-new-zealand-combo-tag-15.jpg', 0, 0, '2019-07-05 23:30:19');

-- --------------------------------------------------------

--
-- Table structure for table `homepage`
--

CREATE TABLE `homepage` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `home_banner_title` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_banner_image` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_banner_features` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_heading` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_description` text COLLATE utf8mb4_unicode_ci,
  `home_section2_title` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_section2_image` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_section2_description` text COLLATE utf8mb4_unicode_ci,
  `home_testimonial_title` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_testimonial_description` text COLLATE utf8mb4_unicode_ci,
  `meta_title` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `homepage`
--

INSERT INTO `homepage` (`id`, `home_banner_title`, `home_banner_image`, `home_banner_features`, `home_heading`, `home_description`, `home_section2_title`, `home_section2_image`, `home_section2_description`, `home_testimonial_title`, `home_testimonial_description`, `meta_title`, `meta_keywords`, `meta_description`, `created_at`, `updated_at`) VALUES
(1, 'Your Plastic<br />Card Solution', 'plastic-cards-newzealand-banner.jpg', '6 Month Warranty,Free Express Delivery,Fast Turnaround,Impressive Service', 'Welcome To Custom Plastic Cards', '<p>Custom Plastic Cards has been a leading plastic card company since 2002. Custom Plastic Cards provides the highest quality plastic cards in the industry, including membership cards, ID cards, gift cards, plastic business cards and the full range of environmentally friendly cards at low prices. Our client base includes individuals and small businesses to high profile private, government and educational institutions. We can print 100 to 3,000,000 cards. The quality and appearance of our cards are sure to have a lasting impact on your customers.</p>', 'Types Of Plastic Cards', 'plastic-cards-newzealand-type-of-plastic-cards.png', '<p>We print the following types of plastic cards: Memberships, Identification, Gift, Discount, Loyalty, Access, Key Tags, Combo cards, Fundraising, Business, Advertising, Novelty, Mailers and More. We also have Environmentally friendly cards made from Teslin, BioPVC or Recycled PVC.</p>', 'See Why Our Clients Love Us', '<p>We received the cards today and are <span>&nbsp;extremely pleased&nbsp;</span> with the <span>&nbsp;quality</span> and <span>&nbsp;fast turnaround&nbsp;</span> of your services. We will continue to use your services for our future needs. I anticipate placing another order within the next 2 weeks. Thank you!</p>', 'Welcome to Custom Plastic Cards : Plastic Cards, Membership Cards, ID Cards - Low Prices, Fast Turnaround', 'Custom Plastic Cards, Custom Plastic Card, Plastic Cards, Plastic Cards', 'Custom Plastic Cards provides the highest quality plastic cards in the industry, including membership cards, ID cards, gift cards, plastic business cards and the full range of environmentally friendly cards at low prices.', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `homepage_boxes`
--

CREATE TABLE `homepage_boxes` (
  `hbox_id` int(11) NOT NULL,
  `hbox_title` varchar(5000) DEFAULT NULL,
  `hbox_image` varchar(1000) DEFAULT NULL,
  `hbox_order` int(11) DEFAULT NULL,
  `hbox_created_at` datetime DEFAULT NULL,
  `hbox_updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `homepage_boxes`
--

INSERT INTO `homepage_boxes` (`hbox_id`, `hbox_title`, `hbox_image`, `hbox_order`, `hbox_created_at`, `hbox_updated_at`) VALUES
(1, 'Environmentally<br />Friendly Cards', '5ce770526001a1558671442.jpg', 1, NULL, NULL),
(2, 'High Quality<br />Cards', '5ce7726e784661558671982.jpg', 2, NULL, NULL),
(3, 'On Time<br />Delivery', '5ce7725fb43451558671967.jpg', 3, NULL, NULL),
(4, '5 Star<br />Service', '5cee2a404bb101559112256.png', 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `homepage_clients`
--

CREATE TABLE `homepage_clients` (
  `clients_id` int(11) NOT NULL,
  `clients_title` varchar(5000) DEFAULT NULL,
  `clients_image` varchar(1000) DEFAULT NULL,
  `clients_order` int(11) DEFAULT NULL,
  `clients_created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `clients_updated_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `homepage_clients`
--

INSERT INTO `homepage_clients` (`clients_id`, `clients_title`, `clients_image`, `clients_order`, `clients_created_at`, `clients_updated_at`) VALUES
(2, 'Plastic cards Newzealand ANZ', 'plastic-cards-newzealand-anz.png', 1, '2019-05-24 10:28:25', '2019-05-29 04:32:02'),
(4, 'Plastic cards Newzealand AGDD', '5cee0a0fbc1911559104015.png', 3, '2019-05-25 21:40:37', '2019-05-29 04:32:02'),
(6, 'Plastic cards Newzealand Europcar', '5cee0a2843c361559104040.png', 5, '2019-05-25 21:41:07', '2019-05-29 04:32:02'),
(8, 'Plastic cards Newzealand Brisbane City', '5cee0ad4191991559104212.png', 4, '2019-05-29 12:30:12', '2019-05-29 04:32:02'),
(9, 'Plastic cards Newzealand BMW', '5cee0b11991401559104273.png', 2, '2019-05-29 12:31:13', '2019-05-29 04:32:02');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_04_22_063916_create_cms_table', 1),
(4, '2019_04_22_083335_create_options_table', 2),
(5, '2019_04_22_084347_alter_cms_table', 2),
(6, '2019_04_22_090213_alter_options_table', 3),
(7, '2019_04_22_091429_alter_options_table', 4),
(8, '2019_04_22_114853_alter_options_table_3', 5),
(9, '2019_05_21_081409_create_pages_table', 5),
(10, '2019_05_21_084554_update_pages_table', 6),
(11, '2019_05_21_093342_update_pages_table_2', 7),
(12, '2019_05_21_095829_update_pages_table_3', 8);

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `youtube` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `googleplus` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phonenumber` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`id`, `youtube`, `linkedin`, `googleplus`, `instagram`, `twitter`, `facebook`, `address`, `email`, `phonenumber`, `mobile`, `created_at`, `updated_at`) VALUES
(1, 'sss', 'lnk', NULL, 'sAS', NULL, NULL, 'Level 26, PWC Tower \r\n188 Quay Street \r\nAuckland 1010 NEW ZEALAND', 'info@plastic-cards.co.nz', '0800 022 737', '099518007', NULL, '2019-09-05 23:24:14');

-- --------------------------------------------------------

--
-- Table structure for table `ordersamples`
--

CREATE TABLE `ordersamples` (
  `ord_id` int(11) NOT NULL,
  `ord_firstname` varchar(100) DEFAULT NULL,
  `ord_lastname` varchar(100) DEFAULT NULL,
  `ord_company` varchar(100) DEFAULT NULL,
  `ord_address1` varchar(5000) DEFAULT NULL,
  `ord_address2` varchar(5000) DEFAULT NULL,
  `ord_city` varchar(200) DEFAULT NULL,
  `ord_state` varchar(200) DEFAULT NULL,
  `ord_zip` varchar(20) DEFAULT NULL,
  `ord_country` varchar(50) DEFAULT NULL,
  `ord_email` varchar(50) DEFAULT NULL,
  `ord_phone` varchar(50) DEFAULT NULL,
  `ord_comments` text,
  `ord_how_findus` varchar(200) DEFAULT NULL,
  `ord_created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ordersamples`
--

INSERT INTO `ordersamples` (`ord_id`, `ord_firstname`, `ord_lastname`, `ord_company`, `ord_address1`, `ord_address2`, `ord_city`, `ord_state`, `ord_zip`, `ord_country`, `ord_email`, `ord_phone`, `ord_comments`, `ord_how_findus`, `ord_created_at`) VALUES
(1, 'Test', 'T', 'company', 'add1', 'add2', 'Clt', 'NSW', '669988', 'Australia', 'test@gmail.com', '9999999999', 'test test', 'Google', '2019-05-27 15:52:55'),
(2, 'Alex Test', 'Test', 'Test company', 'Test', NULL, 'Sydney', 'Nsw', '2766', 'Australia', 'amaniago@clixpert.com.au', '0999888777', 'Test', 'Google', '2019-05-31 07:35:28'),
(3, 'Afbb', 'Test', 'Test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-31 07:35:56'),
(4, 'Alex Test', 'test', 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-31 07:36:21'),
(5, 'Clixpert', 'Test', 'company', 'add1', 'add2', 'Clix', 'NSW', '1545', 'NewZ', 'qacheck17@gmail.com', '9999999999', 'test', 'Google', '2019-07-01 05:09:01'),
(6, 'Alex', 'Test', 'Test Company', '1 Test Rd', NULL, 'Sydney', 'NSW', '2000', 'Australia', 'amaniago@clixpert.com.au', '0999888777', 'test', 'Google', '2019-07-01 16:35:54'),
(7, 'Alex', 'Test 3', 'Test Company', '1 Test St', NULL, 'Sydney', 'NSW', '2000', 'Australia', 'amaniago@clixpert.com.au', '0999888777', 'test', 'Google', '2019-07-01 18:45:11'),
(8, 'Alex Test', 'Test 2', 'Test Company', '1 test Rd', NULL, 'Sydney', 'NSW', '2000', 'Australia', 'amaniago@clixpert.com.au', '0999888777', 'test', 'Email', '2019-07-01 21:53:27'),
(9, 'Clixpert', 'Test', 'Test', 'Test', 'test', 'test', 'test', '22334', 'Test', 'qacheck2018@gmail.com', '1122334455', 'test comment', 'Other', '2019-07-03 22:36:25'),
(10, 'Amanda', 'Whiffen', 'Dunedin Public Libraries', '192 Main Road', 'Waikouaiti', 'Waikouaiti', 'Otago', '9510', 'New Zealand', 'amanda.whiffen@dcc.govt.nz', NULL, 'Please will you send one of each of your corn based, Teslin and recycled plastic cards? I am researching alternatives to our standard plastic library membership cards. I\'d be grateful if you could include information on the longevity of each of these options. Thank you, Amanda', 'Google', '2019-07-30 22:13:17'),
(11, 'bick', 'sing', 'abc pvt', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Google', '2019-09-05 08:21:34'),
(12, 'bick', 'sing', 'abc pvt', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'test', 'Google', '2019-09-05 08:22:30'),
(13, 'test', 'test', 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-05 12:04:25'),
(14, 'test', 'test', 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-05 12:35:23'),
(15, 'Alex Test', 'Test', 'Test Company', '1 Test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-05 17:11:25'),
(16, 'Alex Test', 'Test', 'Test Company', '1 Test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-05 17:12:04'),
(17, 'Alex Test', 'Test', 'Test Company', '1 Test St', NULL, 'Sydney', 'NSW', '2000', 'Australia', 'amaniago@clixpert.com.au', '099988777', 'test', 'Email', '2019-09-05 17:12:46'),
(18, 'Alex Test', 'test', 'Company', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'test', NULL, '2019-09-05 17:13:48'),
(19, 'Alex Test', 'test', 'Company', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'test', NULL, '2019-09-05 17:14:00'),
(20, 'Alex Test', 'test', 'Company', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'test', NULL, '2019-09-05 17:15:25'),
(21, 'Fariz', 'Test', 'Clixpert', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-05 17:22:40'),
(22, 'test', 'test', 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-05 17:58:09'),
(23, 'Fariz', 'Test', 'Clixpert', NULL, NULL, NULL, NULL, NULL, NULL, 'fbey@clixpert.com.au', NULL, NULL, NULL, '2019-09-05 18:26:41'),
(24, 'Fariz', 'Test', 'Clixpert', NULL, NULL, NULL, NULL, NULL, NULL, 'fbey@clixpert', NULL, NULL, NULL, '2019-09-05 18:52:45'),
(25, 'stephanie', 'test', 'Pretty Paws', 'Level 23, 1 Willis Street Wellington,', NULL, NULL, 'wellington', '6011', 'new zealand', 'stephanie@plastic-cards.com.au', NULL, NULL, NULL, '2019-09-05 18:53:53'),
(26, 'Fariz', 'Test', 'Clixpert', NULL, NULL, NULL, NULL, NULL, NULL, 'fbey@clixpert.com.au', NULL, NULL, NULL, '2019-09-05 18:54:03'),
(27, 'Test', 'Test', 'Test', NULL, NULL, NULL, NULL, NULL, NULL, 'test@test.com', NULL, NULL, NULL, '2019-09-05 22:56:36'),
(28, 'Alyssa', 'Entwisle', 'Ravelife Entertainment', '32A George Street', NULL, 'Morrinsville', 'Waikato', '3300', 'New Zealand', 'aj.entwisle@yahoo.co.nz', '0279628005', 'Wanting an ID style card that can be attached to a lanyard for a festival (DJ\'s Crew ETC) Preferably credit card size, doesnt need any fancy features :)', 'Google', '2019-09-30 23:28:29'),
(29, 'Test', 'Test', 'test', NULL, NULL, NULL, NULL, NULL, NULL, 'test@test.com', NULL, 'asasfafa', NULL, '2019-10-28 21:42:55'),
(30, 'Dale', 'Minter', 'TANKMAN®', '52 Makora Avenue', 'Oneroa', 'Waiheke Island', 'Auckland', '1081', 'New Zealand', 'waiheke@tankman.co.nz', '093724442', 'Keytags and Credit Card size, full colour vs 3x colour, hole/s punched, with sequential numbering, embossed as an option. Thanks', 'Google', '2019-12-16 23:56:35');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `page_heading` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_description` text COLLATE utf8mb4_unicode_ci,
  `page_image` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `page_sort_order` int(11) NOT NULL,
  `page_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-Pages, 1-Gallery Page',
  `page_parent` int(11) NOT NULL,
  `page_publish_status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `page_heading`, `page_description`, `page_image`, `meta_title`, `meta_keywords`, `meta_description`, `page_sort_order`, `page_type`, `page_parent`, `page_publish_status`, `created_at`, `updated_at`) VALUES
(1, 'About Us', '<p>Custom Plastic Cards is owned by Sport Software which is a multi-national company with offices in New Zealand, Australia and USA. The company has been established for over 10 years and is focused on designing and producing high quality plastic cards for our valued customers at low prices with short lead times and strict turnaround. We also supply a variety of scanners and swipe card machines.</p><h2>We work with you to produce a design that will impress you.</h2><p><strong>Your choice of colors, from black to multiple colors.</strong></p><ul class=\"listing\"><li>A professional and very impressive finish</li><li>Custom Plastic Cards presentation can include:<ul><li>Photo ID</li><li>Company logos</li><li>Your choice of text, styles and fonts</li></ul></li><li>Signature panel</li><li>Barcode</li><li>Magnetic Stripe (High coarse or Low coarse)</li><li>30, 20 or 10 mm card thickness (30 mm is the standard credit card thickness)</li><li>PVC or composite cards with laminated polish or matte finish</li><li>Print on one or both sides</li><li>Fast turnaround is our specialty</li></ul>', NULL, 'Welcome to Custom Plastic Cards::Plastic Cards, Membership Cards, ID Cards - Low Prices, Fast Turnaround', '', '', 1, 0, 0, 1, NULL, '2019-07-01 17:13:02'),
(2, 'Contact Us', NULL, NULL, 'Contact Custom Plastic Cards, Herne Hill, Victoria, Australia: suppliers of plastic cards', 'plastic cards printing, manufacturing club membership cards, id card suppliers, Australia, Custom Plastic Cards, plastic cards, plastic business cards, discount cards, membership cards, plastic key tags, branded keytags, security cards, gift cards, loyalty cards, access cards, student identification cards, environmentally friendly cards, corn cards, teslin cards, recycled pvc cards, plastic mailers, plastic card manufacturers, plastic card design, online quote, plastic card printing', 'Contact Custom Plastic Cards, based in Herne Hill, Victoria, Australia. We are suppliers of plastic cards, ID cards, membership cards and more. We produce the highest quality plastic cards, including plastic business cards, plastic key tags, discount cards, membership cards, identification cards, access cards, gift cards, loyalty cards and a full range of environmentally friendly cards at low prices. As plastic card manufacturers we print anything from 100 to 3,000,000 cards, with cards designed for your specific needs. Request an online quote for plastic card printing and supply now.', 2, 0, 1, 1, NULL, '2019-05-29 21:32:10'),
(3, 'Testimonials', '<p><strong>Client base includes:</strong> Australian Department of Defence, Brisbane City Council, Sanity records, Europcar, ANZ Bank, BMW, Melbourne University and many more prominent organizations, small businesses and individuals.</p>', NULL, 'Welcome to Custom Plastic Cards::Plastic Cards, Membership Cards, ID Cards - Low Prices, Fast Turnaround', '', '', 3, 0, 1, 1, NULL, '2019-05-29 21:32:59'),
(4, 'Generous Warranty', '<h2>Our Generous Warranty Policy</h2><p>The goal of <strong>Custom Plastic Cards (CPC)</strong> is <strong>100%</strong> customer satisfaction in regard to the quality of our product and customer service. We work hard to give you the best value for money in the industry and that is reflected in the quality and experience of our professional staff. Our pride in our work is reflected in the <strong>30</strong> day warranty, following the delivery of the cards (goods). Our warranty covers the print quality and print content of our plastic cards and covers defects and misprints in this regard.</p><p><strong>CPC</strong> does not guarantee exact color matches unless specific PMS numbers are issued and <strong>CPC</strong> confirms the color matching in writing (usually via email). Warranty is limited to the replacement of defective goods and the approval of <strong>CPC</strong> is required prior to the return of goods. Printing errors on proofs approved by the customer are the customer&#39;s responsibility. <strong>CPC</strong> does not provide a warranty for any damage caused during shipping. We guarantee to ship and invoice &iuml;&iquest;&frac12;10% of the quantity ordered by all customers in regard to plastic cards.</p><p>Prior to the return of our goods, the customer must contact CPC within the warranty period, shall <strong>CPC</strong> decide there are reasonable grounds for a return, <strong>CPC</strong> will replace the goods at <strong>CPC</strong>&#39;s expense. No refund is offered for the shipping on returns in regard to the original order (the order upon which the return is based). If <strong>CPC</strong> decides they cannot meet your requirements for the replacement of the returned goods, <strong>CPC</strong> will refund the cost of the cards and any card set up charges, there will be <strong>no re-stocking</strong> fee applied although no refund is offered for shipping. Refunds may be reduced if the goods are not returned in the original conditions and if the correct card quantity is not returned. You must obtain a return number from us via email prior to returning the goods otherwise the goods will not be accepted.</p>', NULL, 'Welcome to Custom Plastic Cards::Plastic Cards, Membership Cards, ID Cards - Low Prices, Fast Turnaround', '', '', 4, 0, 1, 1, NULL, '2019-05-29 21:34:56'),
(5, 'Services', '<p>Our goal at Custom Plastic Cards is 100% customer satisfaction by providing great customer service and a high quality product. Our friendly sales and support staff work closely with you and give expert guidance to ensure that we exceed your expectations. We use the traditional highest quality PVC cards and environmentally friendly cards including PLA or corn cards, teslin and recycled PVC cards. We have a large range of card types from credit card size cards, key tags and credit card-key tag combinations or alternatively we can produce a card of virtually any shape or size to suit your specific requirements.</p><p>Custom plastic Cards offers CMYK/full color printing and spot/pantone color matching and your choice of numerous card features including sequential and non-sequential numbering, bar coding, magnetic striping, scratch offs with PIN/serial numbers, embossing (raised printing) with gold or silver tipping, signature panels, metallic colored printing, foil stamping, holograms and more. We also offer well qualified design services.</p>', NULL, 'Welcome to Custom Plastic Cards::Plastic Cards, Membership Cards, ID Cards - Low Prices, Fast Turnaround', '', '', 5, 0, 0, 1, NULL, '2019-07-01 17:23:20'),
(6, 'Plastic card Quotation', '<p>Your quote will usually arrive within minutes or call <a href=\"tel:0800022737\" title=\"0800 022 737\">0800 022 737</a> for a quote over the phone with our professional sales staff.</p>', NULL, 'Pricing for printing plastic cards, club membership cards, ID cards, security cards: Custom Plastic Cards, Australia', 'plastic cards printing, manufacturing club membership cards, id card suppliers, Australia, Custom Plastic Cards, plastic cards, plastic business cards, discount cards, membership cards, plastic key tags, branded keytags, security cards, gift cards, loyalty cards, access cards, student identification cards, environmentally friendly cards, corn cards, teslin cards, recycled pvc cards, plastic mailers, plastic card manufacturers, plastic card design, online quote, plastic card printing', 'Request an online quote and pricing for printing plastic cards, club membership cards, ID cards, security cards in Australia. Custom Plastic Cards are manufacturers of the highest quality plastic cards, including plastic business cards, plastic key tags, discount cards, membership cards, security cards, gift cards, loyalty cards and a full range of environmentally friendly cards at low prices. As plastic card manufacturers we print anything from 100 to 3,000,000 cards, with cards designed for your specific needs. Request an online quote for plastic card printing and supply now.', 6, 0, 0, 1, NULL, '2019-06-16 22:51:36'),
(7, 'Gallery', NULL, NULL, 'Welcome to Custom Plastic Cards::Plastic Cards, Membership Cards, ID Cards - Low Prices, Fast Turnaround', '', '', 7, 0, 0, 1, NULL, '2019-05-29 21:39:29'),
(8, 'Credit card size cards', '<p>Our credit card size cards have a vibrant, eye-catching appearance to maximize the impact on your customers. Credit cards size cards are our most popular card type and utilize high quality, long lasting PVC plastic. We also offer a selection of <a href=\"envirocards.html\">environmentally friendly cards</a> cards including certified PLA corn cards, teslin and recycled PVC material. There are numerous features available including full color and spot/pantone color printing, numbering, bar coding, magnetic striping, scratch offs, embossing, signature panels, holograms, smart cards and more.</p>', '5ce9060decb0b1558775309.jpg', 'Welcome to Custom Plastic Cards::Plastic Cards, Membership Cards, ID Cards - Low Prices, Fast Turnaround', NULL, NULL, 8, 1, 7, 1, NULL, '2019-07-06 07:33:08'),
(9, 'Key Tags', '<p>The popularity of key tags continues to grow with their ability to accommodate numerous applications including membership, loyalty, security, promotional programs and more. The attractive and eye-catching appearance of our key tags will impress your customers. We use teslin plastic for our key tags, it is the perfect key tag material, teslin is both <a href=\"envirocards.html\">environmental friendly</a>and ductile, it&#39;s robust nature eliminates snapping, cracking and print fading which are commonly associated with key tags.</p><p>We can also customize the size and shape of your key tag to suit your specific requirements. Numerous features can be added to the key tags including bar codes, numbers, signature panels and more.</p>', '5ce939d79ab101558788567.jpg', 'Welcome to Custom Plastic Cards::Plastic Cards, Membership Cards, ID Cards - Low Prices, Fast Turnaround', '', '', 9, 1, 7, 1, NULL, '2019-05-29 22:12:14'),
(10, 'Combo Cards', '<p>Our combo cards have an eye-catching and a dynamic appearance that will maximize the impact on your clients for your next gift, discount and loyalty project. We use the long lasting and <a href=\"envirocards.html\">environmental friendly</a> teslin plastic for our combos. Combos consist of numerous configurations with 1 credit card size card and 1 to 3 detachable key tags. We can also customize the size and configuration of the combo sets to suit your specific requirements. Numerous features can be added to the combos including bar codes, numbers, signature panels and more.</p><p>We can also customize the size and shape of your key tag to suit your specific requirements. Numerous features can be added to the key tags including bar codes, numbers, signature panels and more.</p>', '5ce93ae3529f11558788835.jpg', 'Welcome to Custom Plastic Cards::Plastic Cards, Membership Cards, ID Cards - Low Prices, Fast Turnaround', NULL, NULL, 10, 1, 7, 1, '2019-05-25 02:58:02', '2019-07-06 06:30:26'),
(11, 'ID Cards', '<p>The use of plastic ID cards - such as photo identification cards or access cards - is becoming increasingly common with the need for security in companies, schools and other organisations, or as part of a club membership card or loyalty card scheme.</p><p>Custom Plastic Cards can design and print a wide range of functional and attractive ID cards that can be used for identification and security access. All of our printed plastic cards come in high quality, long lasting PVC plastic and we can also offer a choice of <a href=\"envirocards.html\">friendly cards.</a></p><p>As an established and experienced ID card printers, we can also offer numerous features for added security or identification, such as card numbering, bar coding, magnetic striping, signature panels, holograms, smart cards and more.</p>', '5cee41a93f2a11559118249.jpg', 'Welcome to Custom Plastic Cards::Plastic Cards, Membership Cards, ID Cards - Low Prices, Fast Turnaround', '', '', 11, 1, 7, 1, '2019-05-25 03:00:30', '2019-07-01 15:41:48'),
(12, 'Plastic Discount Cards', '<p>Plastic Discount cards are the modern and attractive way to represent your shop or brand in style. Any business that doesn&iuml;&iquest;&frac12;t provide these could be left behind nowadays, so impress your customers with a well-designed, high-quality and highly durable discount card to encourage their loyalty.</p><p>Custom Plastic Cards offers CMYK/full colour printing and spot/pantone colour matching as well as qualified design services. You have the choice of numerous card textures and features such as signature panels, magnetic stripes, holograms, smart cards and more, in virtually any shape or size, for an extremely impressive representation of your shop or brand.</p><p>Increase your opportunity to secure your customers&iuml;&iquest;&frac12; loyalty and gain a competitive advantage with a superior plastic discount card from Custom Plastic Cards, the industry experts.</p>', '5cee41e4d0fc31559118308.jpg', 'Welcome to Custom Plastic Cards::Plastic Cards, Membership Cards, ID Cards - Low Prices, Fast Turnaround', '', '', 12, 1, 7, 1, '2019-05-25 03:00:30', '2019-05-29 22:17:33'),
(13, 'Plastic membership Card', '<p>Plastic membership cards are the attractive and durable way of providing your members with a card that they can show with pride. Custom Plastic Cards can produce a quality and stylish plastic card that will last the test of time, in virtually any shape or size to suit your specific requirements.</p><p>Our friendly sales and support staff work closely with you and give expert guidance to ensure that we exceed both your and your members expectations. A well-designed and high-quality membership card is an extremely effective way to impress your members and encourage them to show it to other prospective members.</p><p>A high-quality club deserves a high-quality membership card, so don&iuml;&iquest;&frac12;t settle for anything other than the design and functional excellence that Custom Plastic Cards provides.</p>', '5cee41c30db511559118275.jpg', 'Welcome to Custom Plastic Cards::Plastic Cards, Membership Cards, ID Cards - Low Prices, Fast Turnaround', '', '', 13, 1, 7, 1, '2019-05-25 03:06:43', '2019-05-29 22:18:39'),
(14, 'Custom Cards', '<p>If want that unique card appearance to distinguish you from your competitors, our custom card service could be your best option. We can produce that custom size or shape and design that will stand out from your competitors.</p><p>Just call, email or fax us and we will quote you on exactly what you need. Our friendly sales staff can also discuss your options to ensure that we maximize the impact on your customers. We can use PVC plastic or a range of our <a href=\"envirocards.html\">environmentally friendly cards.</a></p>', '5ce966df832f21558800095.jpg', 'Custom Plastic Cards, Membership Cards, ID Cards - Low Prices, Fast Turnaround | Plastic-cards.co.nz', 'Custom Plastic Cards, Membership Cards, ID Cards', 'If want that unique card appearance to distinguish you from your competitors, our custom card service could be your best option. We can produce that custom size or shape and design that will stand out from your competitors.', 14, 1, 7, 1, '2019-05-25 03:06:43', '2019-09-21 17:31:58'),
(15, 'Mailers', '<p>Our plastic card mailers have produced extraordinary mail out conversion rates. The unique and striking appearance of our plastic mailers commonly produce conversion rates 3 to 10 times the industry standard.</p><p>We use the durable and <a href=\"envirocards.html\" title=\"environmental friendly\">environmental friendly&nbsp;</a> teslin plastic for our mailers. We can also customize the size and configuration of the mailers to maximize your customer conversion. Numerous features can be added to the plastic mailers including bar codes, numbers, signature panels and more.</p>', '5ce967307c32d1558800176.jpg', 'Welcome to Custom Plastic Cards::Plastic Cards, Membership Cards, ID Cards - Low Prices, Fast Turnaround', '', '', 15, 1, 7, 1, '2019-05-25 03:06:43', '2019-07-01 17:20:00'),
(16, 'Order Samples', NULL, NULL, 'Sample plastic cards, suppliers of custom plastic cards for business: Custom Plastic Cards, Australia', 'plastic cards printing, manufacturing club membership cards, id card suppliers, Australia, Custom Plastic Cards, plastic cards, plastic business cards, discount cards, membership cards, plastic key tags, branded keytags, security cards, gift cards, loyalty cards, access cards, student identification cards, environmentally friendly cards, corn cards, teslin cards, recycled pvc cards, plastic mailers, plastic card manufacturers, plastic card design, online quote, plastic card printing', 'Order samples of plastic cards, club membership cards, plastic key tags or ID cards in Australia from Custom Plastic Cards. We produce the highest quality plastic cards, including plastic business cards, plastic key tags, discount cards, membership cards, security cards, gift cards, loyalty cards and a full range of environmentally friendly cards at low prices. As plastic card manufacturers we print anything from 100 to 3,000,000 cards, with cards designed for your specific needs. Request an online quote for plastic card printing and supply now.', 16, 0, 0, 1, '2019-05-25 03:06:43', '2019-05-29 22:22:15'),
(17, 'Specifications for artwork', '<p>Please read through this whole page carefully before submitting art to reduce any delays in getting your order into production.</p><div class=\"fullWdth marbt20\"><table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" class=\"specsTable\"><thead><tr><th>Windows/Mac</th><th>File Format</th><th>DPI</th><th>Mode</th></tr></thead><tbody><tr><td><strong>Illustrator</strong> (outlined)</td><td>.ai</td><td>300+ dpi</td><td>CMYK</td></tr><tr><td><strong>Photoshop</strong> (layers)</td><td>.psd</td><td>300+ dpi</td><td>CMYK</td></tr><tr><td><strong>EPS</strong> (outlined)</td><td>.eps</td><td>300+ dpi</td><td>CMYK</td></tr><tr><td><strong>PDF</strong> (outlined)</td><td>.pdf</td><td>300+ dpi</td><td>CMYK</td></tr><tr><td><strong>Quark</strong> (collected)</td><td>.qdx</td><td><span>300+ dpi</span></td><td><span>CMYK</span></td></tr><tr><td class=\"style46\">InDesign</td><td><span>.indd</span></td><td><span>300+ dpi</span></td><td><span>CMYK</span></td></tr></tbody></table><div class=\"rightDiv\"><p>Please provide artwork as follows:</p><ul class=\"listing\"><li>Design file type in order of preference: Illustrator, eps, psd or PDF</li><li>Resolution of 300 dpi or higher</li><li>Dimension of 3 3/8&quot; x 2 1/8&quot; (add 1/8&quot; bleed)</li><li>Supply artwork as an outline</li></ul></div></div><p><strong>Fonts:</strong> Please include all printer and screen fonts used to create your artwork or convert to outlines.</p><p><strong>Images:</strong> Make sure that you include all of your placed / embedded / imported images when you send your artwork.</p><p><strong>Layers:</strong> Please do not flatten or rasterize your Photoshop files. We need them layered and editable so we can convert the files into outlines to give you high quality prints on your cards.</p><p><strong>Card Options:</strong> Please show all your options on your artwork and state as FPO (for placement only). We will remove all FPO&#39;s barcodes, mag stripes, data, etc.. prior to production.</p><p>Please make sure your artwork is final when presented to us to prevent delay of production.<br>Should you have any questions please feel free to <a href=\"contactus.html\">contact us</a>.</p>', NULL, 'Welcome to Custom Plastic Cards::Plastic Cards, Membership Cards, ID Cards - Low Prices, Fast Turnaround', '', '', 17, 0, 0, 1, '2019-05-25 03:06:43', '2019-05-29 22:23:09'),
(18, 'Templates', '<p>Here you may download templates to create your own artwork.</p><table border=\"1\" cellpadding=\"2\" cellspacing=\"2\" class=\"tempclass\"><tbody><tr><td>Illustrator Template</td><td><a href=\"./public/frontend/images/Illustrator_template.zip\" title=\"CLICK HERE\">CLICK HERE</a></td></tr><tr><td>Photoshop Template</td><td><a href=\"./public/frontend/images/PSDtemplate.zip\" title=\"CLICK HERE\">CLICK HERE</a></td></tr></tbody></table><p>You may also <a href=\"http://www.adobe.com/downloads/\" target=\"_blank\" title=\"download TRIAL versions of Photoshop and Illustrator\">click here</a> to download TRIAL versions of Photoshop and Illustrator</p>', NULL, 'Welcome to Custom Plastic Cards::Plastic Cards, Membership Cards, ID Cards - Low Prices, Fast Turnaround', NULL, NULL, 18, 0, 17, 1, '2019-05-25 03:11:43', '2019-09-24 14:58:57'),
(19, 'Design Services', '<p>For only <strong>$65</strong> our talented designers can design for you a card that will not only promote your event or company but will impress you and your clients.</p><p>All you have to do is give us details of the design you have in mind and we will be happy to get started.</p>', NULL, 'Welcome to Custom Plastic Cards::Plastic Cards, Membership Cards, ID Cards - Low Prices, Fast Turnaround', '', '', 19, 0, 17, 1, '2019-05-25 03:11:43', '2019-05-29 22:28:31'),
(20, 'Submit Order', '<h2 class=\"yu\">IMMEDIATE QUOTATION REQUEST</h2><p>If you do not have the resources to produce your card design our talented and experienced designers can produce an attractive design for you. Please describe what you would like on the front and back of the card in the comments box above and upload any images or logos that you would like to use.We will get to work on your proof and have it to you the same day, if not, the next day in most cases.</p>', NULL, 'Pricing for printing plastic cards, club membership cards, ID cards, security cards: Custom Plastic Cards, Australia', 'plastic cards printing, manufacturing club membership cards, id card suppliers, Australia, Custom Plastic Cards, plastic cards, plastic business cards, discount cards, membership cards, plastic key tags, branded keytags, security cards, gift cards, loyalty cards, access cards, student identification cards, environmentally friendly cards, corn cards, teslin cards, recycled pvc cards, plastic mailers, plastic card manufacturers, plastic card design, online quote, plastic card printing', 'Request an online quote and pricing for printing plastic cards, club membership cards, ID cards, security cards in Australia. Custom Plastic Cards are manufacturers of the highest quality plastic cards, including plastic business cards, plastic key tags, discount cards, membership cards, security cards, gift cards, loyalty cards and a full range of environmentally friendly cards at low prices. As plastic card manufacturers we print anything from 100 to 3,000,000 cards, with cards designed for your specific needs. Request an online quote for plastic card printing and supply now.', 20, 0, 17, 1, '2019-05-25 03:11:43', '2019-05-29 22:35:58'),
(21, 'FAQs', '<p><strong>Q: How do we place a quotation request?</strong><br>A: Simply <a href=\"./quote.htm\">click here</a> and enter your quotation request, it takes very little time.</p><p><strong>Q: Is there a minimum card order?</strong><br>A: Yes, you can order 100 to 10 million cards.</p><p><strong>Q: Do I get to view a proof of the card before production?</strong><br>A: Yes, you will view a proof of the front and back of your card with the specifications. You must approve this proof before card production commences..</p><p><strong>Q: This is my first card order, do you offer advice regarding my card design and the features that I will require?</strong><br>A: Yes, we work with you to create an impressive card design that has the appearance and contains the features that best suits the theme and objectives that you are looking to express.</p><p><strong>Q: If we are happy with the quotation, how do we get started?</strong><br>A: Simply email or call us on our toll free number and we will get the process started immediately.</p>', NULL, 'Welcome to Custom Plastic Cards::Plastic Cards, Membership Cards, ID Cards - Low Prices, Fast Turnaround', '', '', 21, 0, 0, 1, '2019-05-25 03:11:43', '2019-06-18 00:01:22'),
(22, 'Thank You', '<p>Thank you very much for your enquiry, a staff member will contact you shortly with the relevant details.</p>', NULL, 'Welcome to Custom Plastic Cards::Plastic Cards, Membership Cards, ID Cards - Low Prices, Fast Turnaround', NULL, NULL, 22, 0, 0, 1, '2019-05-27 05:40:30', '2019-09-24 14:54:11'),
(23, 'Error 404', NULL, NULL, 'Welcome to Custom Plastic Cards::Plastic Cards, Membership Cards, ID Cards - Low Prices, Fast Turnaround', '', '', 23, 0, 0, 1, '2019-05-28 08:10:02', '2019-05-29 22:37:01'),
(24, 'Business Card', '<p>Plastic business cards are the long-lasting, tastefully designed way to promote your business in style and keep it in your customers&iuml;&iquest;&frac12; mind for many years to come. Custom Plastic Cards can design and print a wide range of functional and attractive business cards. Our talented designers can produce a plastic business card design for you with an eye catching appearance to maximize the impact your business has upon your customers.</p><p>Plastic business cards are very popular nowadays and utilize high quality, long lasting PVC plastic or environmentally friendly materials including certified PLA corn cards, teslin and recycled PVC.</p><p>As a leading plastic business card manufacturer in Australia, Custom Plastic Cards provide a professional style and very impressive finish that can include photo ID, company logos and your choice of text, styles and fonts.</p>', NULL, 'Welcome to Custom Plastic Cards::Plastic Cards, Membership Cards, ID Cards - Low Prices, Fast Turnaround', '', '', 9, 1, 7, 1, '2019-05-29 10:13:04', '2019-05-30 20:58:28'),
(25, 'Environmentally Friendly Cards', '<p>Environmentally friendly cards are fast becoming the ultimate choice for environmentally conscious organizations and individuals. Environmental cards are produced from renewable resources and recycled material, all of which significantly reduces the usage of the limited natural resources on earth, unlike the traditional petroleum base PVC cards. Our suite of environmentally friendly cards includes PLA or corn cards, teslin plastic and recycled PVC cards.</p><hr><p><img src=\"https://plastic-cards.co.nz/public/mediauploads/f9c07f6384804bfee522025c32d78455.jpg\" class=\"fr-fic fr-dii\" title=\"CORN CARDS OR PLA\" alt=\"CORN CARDS OR PLA\"></p><h2>CORN CARDS OR PLA</h2><p>Corn or PLA (poly lactic acid) cards are extracted from corn and are an excellent alternative for environmentally conscious organizations and individuals. Aside from the renewable nature of corn, corn cards require far less energy in production and produce less green house gas than the common petroleum based alternative of PVC. Corn cards are biodegrable and produce no toxins when destroyed during incineration, the ground and atmospheric based environmental advantages of corn cards are significant. The durability, life span and printing of the corn cards is very similar to PVC cards</p><hr><p><img src=\"https://plastic-cards.co.nz/public/mediauploads/57a534d10ec945225242613241cd8261.jpg\" class=\"fr-fic fr-dii\" title=\"TESLIN CARDS\" alt=\"TESLIN CARDS\"></p><h2>TESLIN CARDS</h2><p>Teslin plastic cards are a non-petroleum based synthetic paper product. No harmful toxins are produced during the production of the teslin cards, unlike their petroleum based counterparts. Teslin cards are printed off-set with superior color consistency and are less susceptible to breakage and snapping due to their ductile/elastic properties. Some additional environmental features include: Non-toxicity, no ozone depleting constituents, no cellulose-based material (no forest harvesting), incinerates in an atmosphere of excess oxygen to yield water, carbon dioxide, energy and clean ash (from silica filler).</p><hr><p><img src=\"https://plastic-cards.co.nz/public/mediauploads/bff85d7ece224000585a608743bb503b.jpg\" class=\"fr-fic fr-dii\" title=\"RECYCLED CARDS\" alt=\"RECYCLED CARDS\"></p><h2>RECYCLED CARDS</h2><p>Petroleum based PVC cards are the traditional material for plastic cards. Our recycled cards are produced from recycled PVC. Recycled PVC reduces the amount of PVC that is disposed and produced which reduces the usage of our limited natural resources.</p>', NULL, 'Welcome to Custom Plastic Cards::Plastic Cards, Membership Cards, ID Cards - Low Prices, Fast Turnaround', NULL, NULL, 25, 0, 0, 1, '2019-05-30 04:56:33', '2019-07-02 18:51:30'),
(26, 'Sitemap', '<ul class=\"sitemap\"><li class=\"nav-current\"><a href=\"/\" title=\"Home\">Home</a></li><li class=\"menu-item-has-children\"><a href=\"about-us.html\" title=\"About Us\">About Us</a><ul class=\"sub-menu\"><li><a href=\"contactus.html\" title=\"Contact Us\">Contact Us</a></li><li><a href=\"testimonials.html\" title=\"Testimonials\">Testimonials</a></li><li><a href=\"warranty.html\" title=\"Generous Warranty\">Generous Warranty</a></li></ul></li><li><a href=\"services.html\" title=\"Services\">Services</a></li><li><a href=\"quote.htm\" title=\"Quote\">Quote</a></li><li class=\"menu-item-has-children\"><a href=\"gallery.html\" title=\"Gallery\">Gallery</a><ul class=\"sub-menu\"><li><a href=\"ccardsize.html\" title=\"Credit Card Size\">Credit Card Size</a></li><li><a href=\"keytags.html\" title=\"Key Tags\">Key Tags</a></li><li><a href=\"combos.html\" title=\"Combo Cards\">Combo Cards</a></li><li><a href=\"customcards.html\" title=\"Custom Cards\">Custom Cards</a></li><li><a href=\"mailers.html\" title=\"Mailers\">Mailers</a></li><li><a href=\"ordersamples.html\" title=\"Order Samples\">Order Samples</a></li></ul></li><li class=\"menu-item-has-children\"><a href=\"specs.htm\" title=\"Specs\">Specs</a><ul class=\"sub-menu\"><li><a href=\"templates.html\" title=\"Templates\">Templates</a></li><li><a href=\"designservices.html\" title=\"Design Services\">Design Services</a></li><li><a href=\"submission.asp\" title=\"Submit Order\">Submit Order</a></li></ul></li><li><a href=\"faqs.html\" title=\"FAQ’s\">FAQ&rsquo;s</a></li><li><a href=\"privacy.html\" title=\"Privacy\">Privacy Policy</a></li></ul>', NULL, 'Welcome to Custom Plastic Cards::Plastic Cards, Membership Cards, ID Cards - Low Prices, Fast Turnaround', NULL, NULL, 26, 0, 0, 1, '2019-06-20 11:55:45', '2019-07-02 18:53:54'),
(27, 'Privacy Policy', '<p align=\"justify\">Our organization is committed to the protection of your personal information.<strong>Custom Plastic Cards</strong> will not reveal, disclose, sell, distribute, rent, license, share or pass your personal information on to any third parties without your explicit written consent via email, facsimile or mail. In all cases, your information will be held in strict confidence. The&nbsp;<strong>Custom Plastic Cards</strong> web site has no facility to automatically recognize information regarding the visitor&#39;s domain or email address whilst the visitor is browsing and not deliberately submitting information.</p><p align=\"justify\"><strong>Custom Plastic Cards</strong> will not collect any personal information from you revealing racial or ethnic origin, political opinions, religious or philosophical beliefs, details of health, disability or sexual activity or orientation. Exceptions to this include:</p><p align=\"justify\">* Where you have given express consent to&nbsp;<strong>Custom Plastic Cards</strong> to do so<br>* Where there are reasonable grounds to believe that disclosure is necessary to prevent a threat to life or health<br>* Where the use is authorized by law or reasonably necessary to enforce the law or when the information is necessary for the establishment, exercise or defense of a legal claim</p><p align=\"justify\"><strong>Custom Plastic Cards</strong> may only use your personal information or data for the purpose of responding to your communication, giving support or help related to our product/s, checking the status of the usage of our product/s and direct marketing in relation to promotional activities. Again, your details will not be given to a third party without your consent as described above. You may also notify us at any time if you would like us to destroy your personal information or if you do not wish to receive any further communication from&nbsp;<strong>Custom Plastic Cards</strong>. To do either of the latter two activities go to our Contact Us page (http://www.martialartssoftware.com/order.htm), express your option and include your name and email address.</p><p align=\"center\">If you have any concerns or questions regarding our privacy policy, please contact us Free Call&nbsp;<strong><a href=\"tel:0800%20022%20737\" title=\"0800 022 737\">0800 022 737</a></strong> or email<br><a href=\"mailto:security@plastic-cards.com.nz\">security@</a><a href=\"mailto:security@plastic-cards.com.au\">plastic-cards.com.nz</a> <a href=\"mailto:security@member-cards.com\"></a> or use the&nbsp;<a href=\"https://plastic-cards.co.nz/contactus.html\">Contact Us</a> page.</p>', NULL, 'Welcome to Custom Plastic Cards::Plastic Cards, Membership Cards, ID Cards - Low Prices, Fast Turnaround', '', '', 27, 0, 0, 1, '2019-06-20 12:22:42', '2019-06-20 04:37:46');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `quotation_submission`
--

CREATE TABLE `quotation_submission` (
  `qt_id` int(11) NOT NULL,
  `qt_card_type` varchar(200) DEFAULT NULL,
  `qt_no_of_cards` int(11) DEFAULT NULL,
  `qt_features` varchar(1000) DEFAULT NULL,
  `qt_cardthickness` varchar(50) NOT NULL,
  `qt_no_of_colors_front` varchar(20) DEFAULT NULL,
  `qt_no_of_colors_back` varchar(20) DEFAULT NULL,
  `qt_magnetic_stripe` varchar(20) DEFAULT NULL,
  `qt_date_card` varchar(50) DEFAULT NULL,
  `qt_fullfillment` varchar(5) DEFAULT NULL,
  `qt_how_findus` varchar(500) DEFAULT NULL,
  `qt_firstname` varchar(200) DEFAULT NULL,
  `qt_lastname` varchar(200) DEFAULT NULL,
  `qt_email` varchar(50) DEFAULT NULL,
  `qt_phone` varchar(50) DEFAULT NULL,
  `qt_comments` varchar(5000) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quotation_submission`
--

INSERT INTO `quotation_submission` (`qt_id`, `qt_card_type`, `qt_no_of_cards`, `qt_features`, `qt_cardthickness`, `qt_no_of_colors_front`, `qt_no_of_colors_back`, `qt_magnetic_stripe`, `qt_date_card`, `qt_fullfillment`, `qt_how_findus`, `qt_firstname`, `qt_lastname`, `qt_email`, `qt_phone`, `qt_comments`, `created_at`) VALUES
(1, 'Advertising card', 1222, 'Barcode, Signature Panel, Scratch Off', '0.58 mm thick', '4 Colors', '3 Colors', 'Hi Coercivity', '25-05-2019', NULL, 'Google', 'Test', 'tt', 'test@gmail.com', '00000', 'test test', '2019-05-27 15:08:58'),
(2, 'Advertising card', 12, 'Numbering, Personalization', '0.38 mm thick', '3 Colors', '4 Colors', 'Hi Coercivity', '25-05-2019', NULL, 'Google', 'Test', 'T', 'test@gmail.com', '00000', 'bghfgh', '2019-05-27 15:12:26'),
(3, 'Advertising card', 12, '', '0.76 mm thick', '4 Colors', '1 Color', 'Hi Coercivity', '25-05-2019', NULL, 'Google', 'Test', 'test', 'test@gmail.com', '00000', 'dsadas', '2019-05-27 15:14:55'),
(4, 'Advertising card', NULL, '', '0.76 mm thick', NULL, NULL, 'None', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-29 16:18:45'),
(5, 'Advertising card', NULL, '', '0.76 mm thick', NULL, NULL, 'None', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-29 16:18:47'),
(6, 'Advertising card', NULL, '', '0.76 mm thick', NULL, NULL, 'None', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-29 16:18:48'),
(7, 'Advertising card', NULL, '', '0.76 mm thick', NULL, NULL, 'None', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-29 16:18:48'),
(8, 'Gift card', 2, '', '0.38 mm thick', '2 Colors', '2 Colors', 'Hi Coercivity', '05/10/2019', NULL, 'test', 'tester', 'test', 'test@gmail.com', 'fjg', 'test', '2019-05-29 19:03:10'),
(9, 'Advertising card', 1, 'Barcode', '0.58 mm thick', '1 Color', '1 Color', 'None', '29-05-2019', NULL, 'Google', 'Alex Test', 'Test', 'amaniago@clixpert.com.au', '0999888777', 'Test', '2019-05-31 07:28:19'),
(10, 'Advertising card', 1, 'Numbering', '0.58 mm thick', '1 Color', '1 Color', 'None', '10-06-2019', NULL, 'Google', 'Alex', 'Test', 'amaniago@clixpert.com.au', '0999888777', 'Test', '2019-05-31 07:34:07'),
(11, 'Advertising card', 100, '', '0.76 mm thick', '1 Color', '1 Color', 'Hi Coercivity', '01-07-2019', NULL, 'Google', 'Clixpert', 'Test', 'qacheck17@gmail.com', '000000000', 'test', '2019-07-01 02:53:20'),
(12, 'Advertising card', 100, 'Barcode', '0.76 mm thick', '1 Color', '1 Color', 'Hi Coercivity', '01-07-2019', NULL, 'Google', 'Clixpert', 'Test', 'qacheck17@gmail.com', '000000000', 'test', '2019-07-01 02:55:41'),
(13, 'Advertising card', 100, 'Barcode', '0.76 mm thick', '1 Color', '1 Color', 'Hi Coercivity', '01-07-2019', 'No', 'Google', 'Clixpert', 'Test', 'qacheck17@gmail.com', '000000000', 'tetsetsete', '2019-07-01 03:35:48'),
(14, 'Advertising card', 100, 'Barcode, Numbering', '0.58 mm thick', '1 Color', '1 Color', 'Hi Coercivity', '01-07-2019', 'Yes', 'Google', 'Clixpert', 'Test', 'qacheck17@gmail.com', '000000000', 'test', '2019-07-01 05:01:11'),
(15, 'Advertising card', 1, '', '0.76 mm thick', '1 Color', '1 Color', 'Hi Coercivity', '30-07-2019', 'Yes', 'Google', 'Alex', 'Test', 'amaniago@clixpert.com.au', '0999888777', 'Test', '2019-07-01 16:33:44'),
(16, 'Identification card', 1, '', '0.28 mm thick', '1 Color', '1 Color', 'Hi Coercivity', '08-08-2019', 'No', NULL, 'Alex', 'Test', 'amaniago@clixpert.com.au', '0999888777', 'Test', '2019-07-01 18:43:33'),
(17, 'Membership Card', 50, 'Barcode', '0.76 mm thick', 'Black Only', 'Black Only', 'Hi Coercivity', '01-08-2019', 'No', 'google', 'stephanie', 'test', 'stephmelanie@gmail.com', '123854966', 'test', '2019-07-01 18:58:00'),
(18, 'Discount card', 100, 'Numbering', '0.58 mm thick', '4 Colors', '4 Colors', 'Low Coercivity', '02-08-2019', 'No', 'facebook', 'stephanie', 'test', 'stephmelanie@gmail.com', '564778963', 'test', '2019-07-01 18:59:01'),
(19, 'Gift card', 60, 'Signature Panel', '0.76 mm thick', '1 Color', '1 Color', 'Low Coercivity', '09-08-2019', 'No', 'google', 'stephanie', 'test', 'stephmelanie@gmail.com', '653215844', 'test', '2019-07-01 18:59:57'),
(20, 'Combo Card', 60, 'Personalization', '0.28 mm thick', '2 Colors', '2 Colors', 'Hi Coercivity', '23-08-2019', 'No', 'facebook', 'stephanie', 'test', 'stephmelanie@gmail.com', '632479551', 'test', '2019-07-01 19:01:06'),
(21, 'Environmentally friendly - Recycled PVC', 100, 'Scratch Off', '0.38 mm thick', '3 Colors', '3 Colors', 'Hi Coercivity', '23-08-2019', 'No', 'google', 'stephanie', 'test', 'stephmelanie@gmail.com', '65478932025', 'test', '2019-07-01 19:02:32'),
(22, 'Environmentally friendly - Teslin', 100, 'Barcode', '0.76 mm thick', '4 Colors', '4 Colors', 'Low Coercivity', '30-08-2019', 'No', 'google', 'stephanie', 'test', 'stephmelanie@gmail.com', '1226958', 'test', '2019-07-01 20:06:55'),
(23, 'Combo Card', 60, 'Signature Panel', '0.58 mm thick', '2 Colors', '2 Colors', 'Hi Coercivity', '30-08-2019', 'No', 'google', 'stephanie', 'test', 'stephmelanie@gmail.com', '46558554', 'test', '2019-07-01 20:07:55'),
(24, 'Gift card', 2, '', '0.76 mm thick', 'Black Only', 'Black Only', 'Hi Coercivity', '31-07-2019', 'Yes', NULL, 'ClixpertTest', 'R', 'qacheck2018@gmail.com', '1122334455', 'test', '2019-07-03 22:30:06'),
(25, 'Membership Card', 50, 'Barcode', '0.76 mm thick', '4 Colors', '4 Colors', 'Hi Coercivity', '19-07-2019', 'No', 'google', 'stephanie', 'test', 'stephmelanie@gmail.com', '154896300', 'test', '2019-07-04 22:44:29'),
(26, 'Identification card', 1, '', '0.58 mm thick', '1 Color', '1 Color', 'Hi Coercivity', '31-07-2019', 'Yes', 'Google', 'Alex Test', 'Test', 'amaniago@clixpert.com.au', '0999888777', 'Test', '2019-07-04 23:13:07'),
(27, 'Loyalty card', 2, 'Barcode', '0.58 mm thick', '1 Color', '2 Colors', 'Low Coercivity', '17-07-2019', 'Yes', 'Test', 'Tester', 'T', 'qacheck17@gmail.com', '000000000', 'Test', '2019-07-04 23:50:59'),
(28, 'Loyalty card', 2, '', '0.58 mm thick', '1 Color', '2 Colors', 'Low Coercivity', '17-07-2019', 'Yes', 'Test', 'Tester', 'T', 'qacheck17@gmail.com', '000000000', 'Test', '2019-07-04 23:51:22'),
(29, 'Environmentally friendly - Corn Cards', 22, 'Numbering', '0.28 mm thick', '2 Colors', 'Black Only', 'Low Coercivity', '26-07-2019', 'Yes', 'Test', 'Tester', 't', 'qacheck17@gmail.com', '00000000000000', 'test', '2019-07-05 00:01:59'),
(30, 'Advertising card', 2, 'Numbering', '0.38 mm thick', '1 Color', '1 Color', 'Hi Coercivity', '31-07-2019', 'Yes', 'google', 'Test', 'Test', 'qacheck17@gmail.com', '111111111111', 'test', '2019-07-05 02:24:31'),
(31, 'Key tag', 2, '', '0.58 mm thick', '2 Colors', '1 Color', 'Hi Coercivity', '31-07-2019', 'Yes', 'google', 'Test', 'Test', 'qacheck17@gmail.com', '222222222222', 'test', '2019-07-05 02:25:50'),
(32, 'Membership Card', 1, '', '0.58 mm thick', '1 Color', '2 Colors', 'None', '31-07-2019', 'Yes', 'Google', 'Alextest', 'Test', 'amaniago@clixpert.com.au', '0999888778', 'Test', '2019-07-05 16:35:16'),
(33, 'Membership Card', 1, '', '0.28 mm thick', '3 Colors', '3 Colors', 'Low Coercivity', '10-08-2019', 'Yes', 'Google', 'Alex', 'Test 3', 'amaniago@clixpert.com.au', '0999888777', 'Test', '2019-07-07 18:33:15'),
(34, 'Discount card', 2, 'Barcode, Numbering', '0.58 mm thick', '1 Color', '2 Colors', 'Hi Coercivity', '25-07-2019', 'Yes', 'Test', 'Tester', 'test', 'qacheck17@gmail.com', '000000000', 'Test mail, please ignore', '2019-07-08 23:13:40'),
(35, 'Business card', 1000, '', '0.76 mm thick', '3 Colors', '3 Colors', 'None', '31-07-2019', 'No', NULL, 'Sharon', 'Ward', 'Chair.doc@xtra.co.nz', '021837470', NULL, '2019-07-09 00:28:17'),
(36, 'Discount card', 1, 'Personalization', '0.76 mm thick', '1 Color', '1 Color', 'Hi Coercivity', '30-07-2019', 'Yes', 'Google', 'Alex Test', 'Test 2', 'amaniago@clixpert.com.au', '0999888777', 'Test', '2019-07-09 05:35:46'),
(37, 'Loyalty card', 100, '', '0.76 mm thick', '2 Colors', '1 Color', 'None', '30-08-2019', 'No', NULL, 'Sue', 'Thomas', 'sue@credithouse.co.nz', '06 8350 444', NULL, '2019-07-09 14:43:03'),
(38, 'Business card', 1, '', '0.38 mm thick', '3 Colors', '3 Colors', 'Low Coercivity', '10-08-2019', 'No', 'Google', 'Alex', 'Test 2', 'amaniago@clixpert.com.au', '0999888777', 'Test', '2019-07-09 18:38:17'),
(39, 'Loyalty card', 500, '', '0.76 mm thick', '2 Colors', '1 Color', 'None', NULL, 'No', NULL, 'Sujan', 'Rathod', 'tboinvercargill@gmail.com', '0226574196', NULL, '2019-07-09 21:53:34'),
(40, 'Discount card', 200, 'Personalization', '0.76 mm thick', '2 Colors', '4 Colors', 'None', '23-07-2019', 'No', 'google', 'Caitlin', 'Britton', 'caitlin@showerglasshero.co.nz', NULL, NULL, '2019-07-11 16:36:00'),
(41, 'Membership Card', 1000, 'Personalization', '0.76 mm thick', 'Black Only', 'Black Only', 'Hi Coercivity', NULL, 'No', NULL, 'Hine', 'Lum', 'kohatufit@outlook.com', '0210600388', NULL, '2019-07-18 19:48:33'),
(42, 'Key tag', 50, 'Personalization', '0.76 mm thick', '2 Colors', 'Black Only', 'None', NULL, 'No', 'Google', 'Amanda', 'Mclean', 'Amandafmclean@gmail.com', '0375460003', NULL, '2019-07-25 01:01:23'),
(43, 'Other card type', 50, '', '0.28 mm thick', '2 Colors', '2 Colors', 'None', '26-08-2019', 'No', 'google', 'Sarah', NULL, 'Sarah.Correa@middlemore.co.nz', NULL, NULL, '2019-08-01 18:34:02'),
(44, 'Loyalty card', 2000, '', '0.76 mm thick', '3 Colors', '3 Colors', 'None', '31-08-2019', 'No', 'google', 'Becs', 'McKerchar', 'robinsons@oadunedin.nz', '027 487 7450', NULL, '2019-08-05 16:11:49'),
(45, 'Membership Card', 1500, '', '0.76 mm thick', '3 Colors', 'Black Only', 'Hi Coercivity', NULL, 'No', 'google', 'Tracey', 'Beals', 'operationsmanager@taradalersa.co.nz', '068444808', NULL, '2019-08-08 14:51:54'),
(46, 'Membership Card', 80, '', '0.58 mm thick', '4 Colors', 'Black Only', 'None', '29-02-2020', 'No', 'Google', 'Craig', 'Reynolds', 'accounts@anzfc.co.nz', '021754960', 'Interested in a quote for credit-card-sized plastic membership cards with a photo of the member for a non-profit flying club.  We are talking around 80 or so in February each year, and one off\'s as members join through the year.\r\n\r\nOption for RFID and non-RFID.\r\n\r\nthanks,\r\nCraig', '2019-08-15 19:57:59'),
(47, 'Environmentally friendly - Corn Cards', 200, 'Barcode, Embossing (raised print)', '0.76 mm thick', '2 Colors', '2 Colors', 'None', '03-09-2019', 'No', 'Bing search engine', 'Clifford', 'Wilson', 'clifford@kako.co.nz', '0276944616', 'Not exactly sure what we need re colours etc.  Looking at gift cards ($20 and $50)', '2019-09-17 11:18:00'),
(48, 'Security card', 1000, '', '0.76 mm thick', '1 Color', '1 Color', 'None', NULL, 'No', NULL, 'Test', NULL, 'test@test.com', NULL, 'test message', '2019-09-19 05:19:23'),
(49, 'Membership Card', 10, '', '0.76 mm thick', 'Black Only', 'Black Only', 'None', NULL, 'No', 'Mr google', 'Karen', 'Moffatt-McLeod', 'administration@archerynz.co.nz', '+6421843860', 'these are for our life members - we would like a quote on the environmentally friendly options.  Text and logo on the front - nothing on back, prefer black with white print/logo if possible, if not white with black print/logo', '2019-09-19 20:18:52'),
(50, 'Business card', 1, '', '0.76 mm thick', '1 Color', '1 Color', 'Hi Coercivity', '30-09-2019', 'No', 'Google', 'Alex', 'Test', 'amaniago@clixpert.com.au', '0999888777', 'Test', '2019-09-23 17:09:25'),
(51, 'Identification card', 100, 'Numbering', '0.76 mm thick', 'Black Only', 'Black Only', 'None', '10-10-2019', 'No', 'Google', 'Hadlee', 'Wright', 'venuemanager@turnercentre.co.nz', '0277659139', 'Logo on one side. \r\nText with incremental Number on the back.\r\n(Bar TAB card)', '2019-10-01 15:09:00'),
(52, 'Gift card', 100, 'Signature Panel', '0.76 mm thick', 'Black Only', 'Black Only', 'None', '21-10-2019', 'No', 'Google', 'Bailey', 'Andrews', 'bailey@onceit.co.nz', '0274448705', 'Hi there,\r\n\r\nCan you please provide a quote for 100 gift cards - black and white print only, with space for writing codes/dates/purchased amount on it? I have draft artwork to provide in order to get a more accurate quote.\r\n\r\nThank you,\r\nBailey', '2019-10-13 18:05:06'),
(53, 'Membership Card', 500, 'Numbering', '0.38 mm thick', '4 Colors', 'Black Only', 'None', NULL, 'No', 'Googling', 'Dani', 'Youn', 'youn.haejin@gmail.com', '021168774', 'Numbering from #001 to #500', '2019-10-14 19:47:28'),
(54, 'Membership Card', 500, '', '0.76 mm thick', '2 Colors', '1 Color', 'None', NULL, 'No', NULL, 'Test', NULL, 'test@test.com', NULL, 'sfaf', '2019-10-21 11:50:13'),
(55, 'Advertising card', 300, '', '0.76 mm thick', '3 Colors', '2 Colors', 'None', NULL, 'No', 'GOOGLE', 'Benjamin', 'Marsom', 'benmarsom@gmail.com', '0223673045', NULL, '2019-10-21 15:14:40'),
(56, 'Membership Card', 50, 'Barcode', '0.76 mm thick', 'Black Only', 'Black Only', 'None', '31-10-2019', 'No', 'facebook', 'stephanie', 'test', 'christinemanning83@gmail.com', '4196835920', 'test', '2019-10-22 15:53:35'),
(57, 'Student card', 333, '', '0.76 mm thick', '1 Color', '2 Colors', 'None', NULL, 'No', NULL, 'Hesham', 'Mansour', 'info@member-cards.com', '8668452083', NULL, '2019-10-22 17:38:10'),
(58, 'Membership Card', 5000, 'Barcode, Numbering, Signature Panel', '0.76 mm thick', '2 Colors', 'Black Only', 'None', '08-11-2019', 'No', 'google', 'clay', 'kadius', 'accounts@plastic-cards.com.au', NULL, NULL, '2019-10-22 17:43:12'),
(59, 'Membership Card', 1, 'Barcode', '0.58 mm thick', '1 Color', '2 Colors', 'Hi Coercivity', '31-10-2019', 'Yes', 'Google', 'Alex', 'Test', 'amaniago@clixpert.com.au', '0999888777', 'Test', '2019-10-22 17:58:14'),
(60, 'Membership Card', 100, '', '0.58 mm thick', '1 Color', '1 Color', 'Low Coercivity', '24-10-2019', 'Yes', 'test', 'Fariz', 'Bey', 'fbey@clixpert.com.au', '10415', 'This is a test please ignore', '2019-10-22 17:59:43'),
(61, 'Identification card', 1, '', '0.76 mm thick', '1 Color', '1 Color', 'None', '30-10-2019', 'No', 'Google', 'Alex', 'Test', 'amaniago@clixpert.com.au', '0999888777', 'test', '2019-10-22 17:59:51'),
(62, 'Identification card', 1, 'Numbering', '0.38 mm thick', '1 Color', '2 Colors', 'Hi Coercivity', '29-10-2019', 'Yes', 'Google', 'Alex', 'Test', 'amaniago@clixpert.com.au', '0999888777', 'test', '2019-10-22 18:06:59'),
(64, 'Business card', 250, 'Signature Panel', '0.38 mm thick', '2 Colors', '3 Colors', 'Low Coercivity', '31-10-2019', 'Yes', 'test from clixpert', 'admin clixpert', 'test clixpert', 'clixpert@test.com', '1234567980', 'test only', '2019-10-22 23:14:04'),
(65, 'Gift card', 1, 'Barcode', '0.58 mm thick', '1 Color', '1 Color', 'Hi Coercivity', '30-10-2019', 'Yes', NULL, 'Alex', 'Test', 'test@gmail.com', '0999888777', 'Test', '2019-10-23 15:15:30'),
(66, 'Membership Card', 1, '', '0.76 mm thick', '1 Color', '1 Color', 'Hi Coercivity', '31-10-2019', 'Yes', 'test', 'Alex Test', 'Test 2', 'test@gmail.com', '0999888777', 'Test', '2019-10-23 15:21:30'),
(67, 'Student card', 678, '', '0.76 mm thick', '1 Color', '2 Colors', 'None', NULL, 'No', NULL, 'Hesham', 'Mansour', 'info@member-cards.com', '8668452083', NULL, '2019-10-23 15:21:50'),
(68, 'Membership Card', 1, 'Barcode', '0.76 mm thick', '1 Color', '1 Color', 'None', '29-10-2019', 'No', 'Google', 'Alex', 'Test', 'test@gmail.com', '0999888777', 'Test', '2019-10-23 15:22:45'),
(69, 'Membership Card', 100, 'Barcode', '0.76 mm thick', 'Black Only', 'Black Only', 'Hi Coercivity', '31-10-2019', 'No', 'google', 'stephanie', 'test', 'stephmelanie@gmail.com', '4196835920', 'test', '2019-10-23 15:23:20'),
(70, 'Discount card', 100, 'Barcode', '0.76 mm thick', '4 Colors', '4 Colors', 'Hi Coercivity', '31-10-2019', 'No', 'google', 'stephanie', 'test', 'stephmelanie@gmail.com', '4196835920', 'test', '2019-10-23 17:31:10'),
(71, 'Environmentally friendly - Corn Cards', 444, '', '0.76 mm thick', '1 Color', '1 Color', 'None', NULL, 'No', NULL, 'Hesham', 'Mansour', 'mans446@hotmail.com', '8668452083', NULL, '2019-10-26 01:58:51'),
(72, 'Business card', 1000, 'Barcode', '0.76 mm thick', '1 Color', '1 Color', 'None', NULL, 'No', NULL, 'Test', NULL, 'test@test.com', NULL, NULL, '2019-10-28 21:44:05'),
(73, 'Discount card', 1, '', '0.58 mm thick', '1 Color', '1 Color', 'Hi Coercivity', '31-10-2019', 'No', 'Google', 'Alex Test', 'Test', 'amaniago@clixpert.com.au', '0999888777', 'Test', '2019-10-28 22:03:35'),
(74, 'Identification card', 1, '', '0.28 mm thick', '1 Color', '1 Color', 'Hi Coercivity', '30-10-2019', 'Yes', 'Google', 'Alex Test', 'Test', 'amaniago@clixpert.com.au', '0999888777', 'Test', '2019-10-28 22:04:09'),
(75, 'Membership Card', 100, '', '0.76 mm thick', '2 Colors', '2 Colors', 'Hi Coercivity', NULL, 'No', NULL, 'Ash', 'Sharma', 'ash@esignwebservices.in', '9718099999', 'This is test quotation. Thanks', '2019-10-29 22:48:50'),
(76, 'Gift card', 250, 'Barcode', '0.76 mm thick', '2 Colors', '2 Colors', 'None', NULL, 'No', NULL, 'Liz', 'Reid', 'liz@studiomatakana.co.nz', '021361063', NULL, '2019-10-30 18:11:32'),
(77, 'Business card', 20, 'Numbering', '0.76 mm thick', '1 Color', '1 Color', 'None', '22-11-2019', 'No', 'google', 'Anna', 'Mackintosh', 'anna@bannockburnhotel.com', '0275550786', NULL, '2019-11-05 17:40:43'),
(78, 'Combo Card', 500, 'Barcode, Numbering', '0.76 mm thick', '3 Colors', '1 Color', 'None', '20-11-2019', 'No', 'Google', 'Adam', 'Pegler', 'arpegler@gmail.com', '0212120474', NULL, '2019-11-05 23:47:43'),
(79, 'Business card', 100, '', '0.58 mm thick', '2 Colors', '2 Colors', 'None', '30-11-2019', 'No', 'Google', 'Alecia', 'Stanford-', 'aleciastanford@me.com', '021341733', 'Hi, i emailled for a quote last week. i have not had anything yet. I would like to emboss logo on the front of the card and printed on the back. Is this possible? \r\n\r\nThanks', '2019-11-06 18:32:35'),
(80, 'Gift card', 1, '', '0.76 mm thick', 'None', 'None', 'Low Coercivity', '30-11-2019', 'No', 'facebook', 'stephanie', 'test', 'stephmelanie@gmail.com', '4196835920', 'test', '2019-11-07 15:38:33'),
(81, 'Membership Card', 1, 'Barcode', '0.76 mm thick', '4 Colors', '4 Colors', 'Hi Coercivity', '30-11-2019', 'No', 'facebook', 'stephanie', 'test', 'stephmelanie@gmail.com', '4196835920', 'test', '2019-11-07 15:39:11'),
(82, 'Gift card', 50, '', '0.76 mm thick', '4 Colors', '4 Colors', 'None', NULL, 'No', NULL, 'David', 'Kennedy', 'swanson.admin@f45training.co.nz', NULL, 'Do you also do gift card holders/wallets?', '2019-12-01 12:38:19'),
(83, 'Key tag', 500, '', '0.76 mm thick', '2 Colors', '2 Colors', 'None', NULL, 'No', 'Google', 'Jamie', 'Burrows', 'jamie@slsgroup.co.nz', '021 497748', NULL, '2019-12-04 16:14:13'),
(84, 'Membership Card', 20, '', '0.76 mm thick', '2 Colors', '2 Colors', 'None', '11-01-2020', 'No', 'google', 'Paul', 'Coan', 'paul@racingsims.co.nz', '0275208287', NULL, '2019-12-09 20:40:23'),
(85, 'Key tag', 200, '', '0.76 mm thick', '2 Colors', '1 Color', 'None', '20-12-2019', 'No', 'online', 'Andy', 'Zhang', 'andyz@remarkablegifts.co.nz', '0211900688', NULL, '2019-12-09 21:40:13'),
(86, 'Membership Card', 4, 'Numbering', '0.76 mm thick', '2 Colors', '3 Colors', 'None', '15-01-2020', 'No', 'Enternet', 'Herman', 'Jay', 'drhermanjay@gmail.com', '0210756896', NULL, '2019-12-10 21:57:51'),
(87, 'Business card', 500, '', '0.76 mm thick', '2 Colors', 'Black Only', 'None', '16-01-2019', 'No', 'Google', 'Trish', 'Shearer', 'Trish@delishheels.co.nz', '0273354777', 'Can I please have a qoute ty', '2019-12-15 18:08:56'),
(88, 'Business card', 100, '', '0.76 mm thick', '2 Colors', '2 Colors', 'None', '06-01-2020', 'Yes', NULL, 'scott', 'oconnor', 'scott.oconnor@fourpoints.com', '0211956508', NULL, '2019-12-16 22:29:59'),
(89, 'Key tag', 250, 'Numbering', '0.76 mm thick', '3 Colors', 'None', 'None', '01-01-2020', 'No', 'Google', 'Dale', 'Minter', 'waiheke@tankman.co.nz', '093724442', 'We want to rivet a plastic keytag to water tanks. It will have our logo and a sequential number for identification purposes in future. Is the print and plastic suitable for outdoor use / UV resistant? Thanks, Dale', '2019-12-16 23:54:56'),
(90, 'Membership Card', 100, 'Numbering, Scratch Off', '0.76 mm thick', '1 Color', 'Black Only', 'None', '31-12-2019', 'No', 'google', 'Erica', 'Minter', 'erica.g.minter@gmail.com', '021467750', 'unique numbering on the front and scratchoff 6 code on the back; \r\nnumber & cdoe combos to be provided to us for backend processing', '2019-12-17 20:09:11'),
(91, 'Membership Card', 500, 'Barcode, Numbering, Signature Panel, Personalization', '0.76 mm thick', '2 Colors', '1 Color', 'None', NULL, 'No', NULL, 'Yifei', NULL, 'yifei1077@gmail.com', NULL, NULL, '2019-12-31 18:52:22'),
(92, 'Gift card', 250, 'Barcode', '0.76 mm thick', '2 Colors', 'Black Only', 'None', NULL, 'No', 'Google', 'Anaru', 'White', 'info@taranakigolfcentre.co.nz', '0211965916', NULL, '2020-01-05 14:53:47'),
(93, 'Business card', 1000, '', '0.38 mm thick', 'Black Only', 'Black Only', 'None', '08-02-2020', 'No', 'Google', 'Acacia', 'Nightingale', 'acacia@greenroomdesign.co.nz', '0226378503', 'Good Morning,\r\n\r\nI am looking for a quote on behalf of one of my clients who is wanting to have 1000 plastic business cards printed. The plastic card would be white with double sided black only printing. The corners of the card need to be rounded/diecut. Please let me know what you need from me in order for me to obtain a full quote. Also please advise what the expected turn around time would be on an order like this so that I can let my client know. Lastly, are you able to send out samples of the plastic card weight as my client wants something thinner and more flexible than a credit card or loyalty card.\r\n\r\nThanks in advance,\r\nAcacia', '2020-01-08 15:47:31'),
(94, 'Discount card', 100, '', '0.76 mm thick', '4 Colors', '4 Colors', 'None', NULL, 'No', NULL, 'Nathan', 'Jennings', 'nathan.jennings@printing.com', '02742445593', '250 / 500 / 1000 Please quote additional quantities', '2020-01-13 16:15:23'),
(95, 'Business card', 56, '', '0.76 mm thick', '2 Colors', '1 Color', 'Hi Coercivity', NULL, 'Yes', 'Google', 'sandeep - clixpert test', 'Test', 'manchikantisandeep123@gmail.com', '9700902204', 'ignore and just test', '2020-01-19 05:55:17'),
(96, 'Membership Card', 1000, '', '0.76 mm thick', '3 Colors', 'None', 'None', NULL, 'No', 'Google', 'Atanas', 'Tomovski', 'vuwlss@gmail.com', '0211876595', NULL, '2020-01-20 02:22:11'),
(97, 'Membership Card', 1, 'Personalization', '0.58 mm thick', 'Black Only', 'Black Only', 'Hi Coercivity', '31-01-2020', 'No', 'Google', 'Clixpert Regular', 'Maintenance Check', 'amaniago@clixpert.com.au', '0000 000 000', 'Please ignore', '2020-01-20 15:01:33'),
(98, 'Membership Card', 12, 'Barcode', '0.58 mm thick', '1 Color', '1 Color', 'Hi Coercivity', '29-01-2020', 'Yes', NULL, 'clixpert test1', 'clixpert test1', 'clixpertmaintanance@gmail.com', '011022033', 'test1', '2020-01-20 22:19:17'),
(100, 'Loyalty card', 2, 'Barcode', '0.58 mm thick', '3 Colors', '3 Colors', 'Low Coercivity', '25-01-2020', 'Yes', 'Test', 'Test', 'Test', 'clixpermaintanance@gmail.com', '011022033', 'Form Test', '2020-01-20 23:55:53'),
(101, 'Gift card', 15, 'Barcode', '0.76 mm thick', '2 Colors', '2 Colors', 'Hi Coercivity', '31-01-2020', 'No', 'Google', 'Fariz', 'Bey', 'fbey@clixpert.com.au', '0415', 'Maintenance check. Clixpert', '2020-01-23 21:54:58'),
(102, 'Identification card', 100, '', '0.58 mm thick', 'Black Only', 'Black Only', 'Hi Coercivity', '31-01-2020', 'Yes', 'Google', 'Ranjan', 'Ratnam', 'rratnam@clixpert.com.au', '043111158', 'Just checking the form please do not reply', '2020-01-29 19:46:46'),
(103, 'Business card', 200, '', '0.76 mm thick', 'Black Only', 'Black Only', 'None', '10-02-2020', 'No', 'internet', 'melanie', 'tan', 'teamcavekiely@raywhite.com', '02102580311', NULL, '2020-02-02 19:19:04'),
(104, 'Membership Card', 100, '', '0.76 mm thick', '2 Colors', '2 Colors', 'None', '16-03-2020', 'No', 'Google', 'Isabel', NULL, 'isabel.h@moderentals.co.nz', NULL, NULL, '2020-02-03 18:45:18'),
(105, 'Business card', 250, 'Personalization', '0.76 mm thick', 'Black Only', 'Black Only', 'None', '01-04-2020', 'No', NULL, 'Laura', 'Malcolm', 'laura@naturalnzhoney.co.nz', '+64273049116', 'Plastic business cards for bus company.', '2020-02-05 20:16:02'),
(106, 'Membership Card', 100, 'Barcode, Numbering', '0.58 mm thick', '3 Colors', 'Black Only', 'None', '03-03-2020', 'No', 'Search', 'Rochelle', 'Birks', 'rochellebirks@live.com', '027220924', NULL, '2020-02-09 11:03:26'),
(107, 'Membership Card', 250, 'Numbering, Personalization', '0.76 mm thick', '2 Colors', 'Black Only', 'None', '01-04-2020', 'No', 'Google', 'Jack', 'Morris', 'Onelessheadache28@gmail.com', '0278567358', NULL, '2020-02-09 19:58:46');

-- --------------------------------------------------------

--
-- Table structure for table `slug`
--

CREATE TABLE `slug` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `slug_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `template` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slug`
--

INSERT INTO `slug` (`id`, `slug_url`, `template`, `page_id`, `created_at`, `updated_at`) VALUES
(1, 'about-us.html', 'pages', 1, NULL, '2019-05-22 06:00:18'),
(2, 'contactus.html', 'contactus', 2, NULL, NULL),
(3, 'testimonials.html', 'testimonial', 3, NULL, NULL),
(4, 'warranty.html', 'pages', 4, NULL, NULL),
(5, 'services.html', 'pages', 5, NULL, NULL),
(6, 'quote.htm', 'quote', 6, NULL, NULL),
(7, 'gallery.html', 'gallery', 7, NULL, NULL),
(8, 'ccardsize.html', 'gallery_detail', 8, NULL, '2019-05-23 00:57:01'),
(9, 'keytags.html', 'gallery_detail', 9, NULL, NULL),
(10, 'combos.html', 'gallery_detail', 10, '2019-05-25 02:58:02', '2019-05-25 02:58:02'),
(11, 'idcards.html', 'gallery_detail', 11, '2019-05-25 03:00:30', '2019-05-25 03:00:30'),
(12, 'discountcards.html', 'gallery_detail', 12, '2019-05-25 03:00:30', '2019-05-25 03:00:30'),
(13, 'membershipcards.html', 'gallery_detail', 13, '2019-05-25 03:06:43', '2019-05-25 03:06:43'),
(14, 'customcards.html', 'gallery_detail', 14, '2019-05-25 03:06:43', '2019-05-25 03:06:43'),
(15, 'mailers.html', 'gallery_detail', 15, '2019-05-25 03:06:43', '2019-05-25 03:06:43'),
(16, 'ordersamples.html', 'ordersamples', 16, '2019-05-25 03:06:43', '2019-05-25 03:06:43'),
(17, 'specs.htm', 'pages', 17, '2019-05-25 03:06:43', '2019-05-25 03:06:43'),
(18, 'templates.html', 'pages', 18, '2019-05-25 03:11:43', '2019-05-25 03:11:43'),
(19, 'designservices.html', 'pages', 19, '2019-05-25 03:11:43', '2019-05-25 03:11:43'),
(20, 'submission.asp', 'submitorder', 20, '2019-05-25 03:11:43', '2019-05-25 03:11:43'),
(21, 'faqs.html', 'pages', 21, '2019-05-25 03:11:43', '2019-05-25 03:11:43'),
(22, 'thank-you.html', 'pages', 22, '2019-05-27 05:40:56', '2019-05-27 05:40:56'),
(23, 'businesscards.html', 'gallery_detail', 24, '2019-05-29 10:20:31', '2019-05-29 10:20:31'),
(24, 'envirocards.html', 'pages', 25, '2019-05-30 04:57:54', '2019-05-30 04:57:54'),
(25, 'sitemap.html', 'pages', 26, '2019-06-20 11:51:51', '2019-06-20 11:51:51'),
(26, 'privacy.html', 'pages', 27, '2019-06-20 12:25:50', '2019-06-20 12:25:50');

-- --------------------------------------------------------

--
-- Table structure for table `submit_order_submissions`
--

CREATE TABLE `submit_order_submissions` (
  `sbo_id` int(11) NOT NULL,
  `sbo_firstname` varchar(100) DEFAULT NULL,
  `sbo_lastname` varchar(100) DEFAULT NULL,
  `sbo_email` varchar(500) DEFAULT NULL,
  `sbo_phone` varchar(100) DEFAULT NULL,
  `sbo_upload` varchar(200) DEFAULT NULL,
  `sbo_comments` text,
  `con_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `submit_order_submissions`
--

INSERT INTO `submit_order_submissions` (`sbo_id`, `sbo_firstname`, `sbo_lastname`, `sbo_email`, `sbo_phone`, `sbo_upload`, `sbo_comments`, `con_date`) VALUES
(1, 'Test', 'testname', 'test@gmail.com', '000000000', '5ceb9a3873d2e1558944312.pdf', 'testesttest', '2019-05-27 13:35:12'),
(2, 'aaa', 'sss', 'test@gmail.com', '9999', '5ceb9aeb0814f1558944491.pdf', 'gggg', '2019-05-27 13:38:11'),
(3, 'bbb', 'ff', 'test@gmail.com', '000000000', '5ceb9b959f3c41558944661.pdf', NULL, '2019-05-27 13:41:01'),
(4, 'gg', 'hh', 'gdsgs@gmail.com', '23442', '5ceb9bc6c43171558944710.pdf', NULL, '2019-05-27 13:41:50'),
(5, 'Test', 'T', 'test@gmail.com', '000000000', '5cee1a25a02a21559108133.pdf', 'test', '2019-05-29 13:35:33'),
(6, 'Test', 'test', 'test@gmail.com', NULL, '5cee6a06862731559128582.txt', 'test', '2019-05-29 19:16:22'),
(7, 'Alex Test', 'Test', 'amaniago@clixpert.com.au', '0999888777', '', 'test', '2019-05-31 07:29:57'),
(8, 'Clixpert Test', 'T', 'qacheck17@gmail.com', '000000000', '5d19f98b5ffde1561983371.jpg', 'test tset', '2019-07-01 05:16:11'),
(9, 'Alex Test', 'Test 2', 'amaniago@clixpert.com.au', '0999888888', '', 'Test', '2019-07-01 05:23:45'),
(10, 'Clixpert', 'Test', 'qacheck2018@gmail.com', '1122334455', '', 'test comment', '2019-07-03 22:43:51'),
(11, 'Alex Test', 'Test', 'amaniago@clixpert.com.au', '0999888777', '', 'Test', '2019-07-04 23:15:57');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `tes_id` int(11) NOT NULL,
  `tes_title` varchar(100) DEFAULT NULL,
  `tes_description` text,
  `tes_order` int(11) DEFAULT '0',
  `tes_created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `tes_updated_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`tes_id`, `tes_title`, `tes_description`, `tes_order`, `tes_created_at`, `tes_updated_at`) VALUES
(1, 'Rick King, Rick\'s Place', 'Absolute top notch products and 5 star service. We highly recommend <strong>Custom Plastic Cards.</strong>', 1, '2019-05-25 21:12:22', '2019-05-25 21:12:22'),
(2, 'Jim Harding, CA', 'My experience with <strong>Custom Plastic Cards</strong> was outstanding! My order required numerical sequencing on the front and back of the card. I submitted my design via fax, clarified a few points over the phone, and within a few days I had the finished product delivered to me. These high quality cards were exactly what I was expecting. Along with the great service and low cost I would not hesitate to recommend <strong>Custom Plastic Cards.</strong>', 2, '2019-05-25 21:12:53', '2019-05-25 21:12:53'),
(3, 'Brandon Trentler, Operations Manager, Park1', '<strong>Custom Plastic Cards</strong> did a fantastic job on our order! They worked with us to ensure that the cards we received were exactly what we wanted. Their quality showed in the work they did. I was also impressed at the speed in which we received our cards. I will certainly use <strong>Custom Plastic Cards</strong> for access cards again.', 3, '2019-05-25 21:15:20', '2019-05-25 21:15:20'),
(4, 'Lexington Golf Club, Lexington', 'Our golf club implemented a swipe card accounting system. Finding magnetic plastic cards to interact with this system fell on my shoulders. I searched extensively for a company to fulfill my needs but I had a difficult time just finding a company that responded. I did a Google search and located <strong>Custom Plastic Cards</strong>. From the first phone conversation to the last, <strong>Custom Plastic Cards</strong> were great to do business with. The response time was unbelievable and the end product was exactly what I needed at a great price. I promptly received my cards from the time of my initial conversation, the printed and encoded cards were perfectly done. I can say that I highly recommend the <strong>Custom Plastic Cards</strong> Company.', 4, '2019-05-25 21:15:42', '2019-05-25 21:15:42'),
(5, 'Brian D. Schram, President, Hennessy & Associates', 'As a repeat customer of <strong>Custom Plastic Cards,</strong> I am extremely happy with the quality of my orders. Each order has been completed on time and accurately. Perhaps the greatest satisfaction comes from the friendly, hands-on service provided by each employee.', 5, '2019-05-25 21:16:06', '2019-05-25 21:16:06'),
(6, 'Kristin Hugo, Owner, Dream Haven Interiors', 'We are proud to issue the cards we received from <strong>Custom Plastic Cards</strong> to our customers. The staff worked diligently with us to get the look we wanted and promptly responded to all emails and telephone calls. We received the cards within days. We are extremely happy with the product and the service of <strong>Custom Plastic Cards</strong> and would recommend them to anybody in the market for custom cards.', 6, '2019-05-25 21:16:26', '2019-05-25 21:16:26'),
(7, 'Michael Fisher, NSW', '(Actual Email)\r\nWe received the cards today and are extremely pleased with the quality and fast turnaround of your services. We will continue to use your services for our future needs. I anticipate placing another order within the next 2 weeks. Thank you!', 7, '2019-05-25 21:16:48', '2019-05-25 21:16:48'),
(8, 'Tracey S', 'We have been a customer since 2011 and continue returning due to the continued professional customer service we receive from Hesham! Always a delight to deal with, quick and efficient dealings via email. Great product leaving our customers happy. Definitely will continue ordering here & highly recommend this company.', 8, '2019-06-21 13:30:21', '2019-06-21 13:30:21'),
(9, 'Crownalee Van', 'Karen was very efficient and professional in the way she handled our orders. The products are of high quality and impressive. Definitely will use them again! Highly recommended!', 9, '2019-06-21 13:31:11', '2019-06-21 13:31:11'),
(10, 'David Allan', 'I have been in the industry for 20 years and have used these guys for 8 years now, as they have the best quality cards I have seen. They provide great advice and a reliable service all round, and at a reasonable price to boot.', 10, '2019-06-21 13:31:51', '2019-06-21 13:31:51'),
(11, 'Cindy Marron', 'I use this company since 2011 for all my custom plastic cards. Great quality and delivery is always on time.', 11, '2019-06-21 13:32:43', '2019-06-21 13:32:43');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'CustomPlastics', 'admin@plasticcard', NULL, '$2y$10$Qj51dFX1V0KeM2PILT5v0eMhiBJjodIx5.n.ZPiaQteKOFF.Fu7F.', 'EQKKpnrd4bJs94R2eUWeZvVNO2GUpBYGiOuzBhjH9hDDfENjGqhbRxUgyGQR', '2019-04-22 03:58:12', '2019-04-22 03:58:12');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contact_submissions`
--
ALTER TABLE `contact_submissions`
  ADD PRIMARY KEY (`con_id`);

--
-- Indexes for table `cpau_cmsv_delete`
--
ALTER TABLE `cpau_cmsv_delete`
  ADD PRIMARY KEY (`cms_id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`gallery_id`);

--
-- Indexes for table `homepage`
--
ALTER TABLE `homepage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `homepage_boxes`
--
ALTER TABLE `homepage_boxes`
  ADD PRIMARY KEY (`hbox_id`);

--
-- Indexes for table `homepage_clients`
--
ALTER TABLE `homepage_clients`
  ADD PRIMARY KEY (`clients_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ordersamples`
--
ALTER TABLE `ordersamples`
  ADD PRIMARY KEY (`ord_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `quotation_submission`
--
ALTER TABLE `quotation_submission`
  ADD PRIMARY KEY (`qt_id`);

--
-- Indexes for table `slug`
--
ALTER TABLE `slug`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `submit_order_submissions`
--
ALTER TABLE `submit_order_submissions`
  ADD PRIMARY KEY (`sbo_id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`tes_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contact_submissions`
--
ALTER TABLE `contact_submissions`
  MODIFY `con_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `cpau_cmsv_delete`
--
ALTER TABLE `cpau_cmsv_delete`
  MODIFY `cms_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `gallery_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=227;

--
-- AUTO_INCREMENT for table `homepage`
--
ALTER TABLE `homepage`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `homepage_boxes`
--
ALTER TABLE `homepage_boxes`
  MODIFY `hbox_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `homepage_clients`
--
ALTER TABLE `homepage_clients`
  MODIFY `clients_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ordersamples`
--
ALTER TABLE `ordersamples`
  MODIFY `ord_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `quotation_submission`
--
ALTER TABLE `quotation_submission`
  MODIFY `qt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `slug`
--
ALTER TABLE `slug`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `submit_order_submissions`
--
ALTER TABLE `submit_order_submissions`
  MODIFY `sbo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `tes_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
